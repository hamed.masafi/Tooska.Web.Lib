﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tooska.Payment.ir.shaparak.bpm;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Globalization;

// https://bpm.shaparak.ir/pgwchannel/services/pgw?wsdl
namespace Tooska.Payment
{
    public class Behardakht<T> : AbstractPaymentGateway<T> where T : AbstractTransaction
    {
        //long terminalId = 2048947;
        //string userName = "pmkala";
        //string userPassword = "36870858";
        public static long terminalId { get; set; }
        public static string userName { get; set; }
        public static string userPassword { get; set; }

        const int SUCCESS = 0;

        public enum MellatBankReturnCode
        {
            ﺗﺮاﻛﻨﺶ_ﺑﺎ_ﻣﻮﻓﻘﻴﺖ_اﻧﺠﺎم_ﺷﺪ = 0,
            ﺷﻤﺎره_ﻛﺎرت_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 11,
            ﻣﻮﺟﻮدي_ﻛﺎﻓﻲ_ﻧﻴﺴﺖ = 12,
            رﻣﺰ_ﻧﺎدرﺳﺖ_اﺳﺖ = 13,
            ﺗﻌﺪاد_دﻓﻌﺎت_وارد_ﻛﺮدن_رﻣﺰ_ﺑﻴﺶ_از_ﺣﺪ_ﻣﺠﺎز_اﺳﺖ = 14,
            ﻛﺎرت_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 15,
            دﻓﻌﺎت_ﺑﺮداﺷﺖ_وﺟﻪ_ﺑﻴﺶ_از_ﺣﺪ_ﻣﺠﺎز_اﺳﺖ = 16,
            ﻛﺎرﺑﺮ_از_اﻧﺠﺎم_ﺗﺮاﻛﻨﺶ_ﻣﻨﺼﺮف_ﺷﺪه_اﺳﺖ = 17,
            ﺗﺎرﻳﺦ_اﻧﻘﻀﺎي_ﻛﺎرت_ﮔﺬﺷﺘﻪ_اﺳﺖ = 18,
            ﻣﺒﻠﻎ_ﺑﺮداﺷﺖ_وﺟﻪ_ﺑﻴﺶ_از_ﺣﺪ_ﻣﺠﺎز_اﺳﺖ = 19,


            ﺻﺎدر_ﻛﻨﻨﺪه_ﻛﺎرت_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 111,
            ﺧﻄﺎي_ﺳﻮﻳﻴﭻ_ﺻﺎدر_ﻛﻨﻨﺪه_ﻛﺎرت = 112,
            ﭘﺎﺳﺨﻲ_از_ﺻﺎدر_ﻛﻨﻨﺪه_ﻛﺎرت_درﻳﺎﻓﺖ_ﻧﺸﺪ = 113,
            دارﻧﺪه_ﻛﺎرت_ﻣﺠﺎز_ﺑﻪ_اﻧﺠﺎم_اﻳﻦ_ﺗﺮاﻛﻨﺶ_ﻧﻴﺴﺖ = 114,


            ﭘﺬﻳﺮﻧﺪه_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 21,
            ﺧﻄﺎي_اﻣﻨﻴﺘﻲ_رخ_داده_اﺳﺖ = 23,
            اﻃﻼﻋﺎت_ﻛﺎرﺑﺮي_ﭘﺬﻳﺮﻧﺪه_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 24,
            ﻣﺒﻠﻎ_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 25,
            ﭘﺎﺳﺦ_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 31,
            ﻓﺮﻣﺖ_اﻃﻼﻋﺎت_وارد_ﺷﺪه_ﺻﺤﻴﺢ_ﻧﻤﻲ_ﺑﺎﺷﺪ = 32,
            ﺣﺴﺎب_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 33,
            ﺧﻄﺎي_ﺳﻴﺴﺘﻤﻲ = 34,
            ﺗﺎرﻳﺦ_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 35,
            ﺷﻤﺎره_درﺧﻮاﺳﺖ_ﺗﻜﺮاري_اﺳﺖ = 41,
            ﺗﺮاﻛﻨﺶ_Sale_یافت_نشد_ = 42,
            ﻗﺒﻼ_Verify_درﺧﻮاﺳﺖ_داده_ﺷﺪه_اﺳﺖ = 43,
            درخواست_verify_یافت_نشد = 44,
            ﺗﺮاﻛﻨﺶ_Settle_ﺷﺪه_اﺳﺖ = 45,
            ﺗﺮاﻛﻨﺶ_Settle_نشده_اﺳﺖ = 46,
            ﺗﺮاﻛﻨﺶ_Settle_یافت_نشد = 47,
            تراکنش_Reverse_شده_است = 48,
            تراکنش_Refund_یافت_نشد = 49,


            شناسه_قبض_نادرست_است = 412,
            ﺷﻨﺎﺳﻪ_ﭘﺮداﺧﺖ_ﻧﺎدرﺳﺖ_اﺳﺖ = 413,
            سازﻣﺎن_ﺻﺎدر_ﻛﻨﻨﺪه_ﻗﺒﺾ_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 414,
            زﻣﺎن_ﺟﻠﺴﻪ_ﻛﺎري_ﺑﻪ_ﭘﺎﻳﺎن_رسیده_است = 415,
            ﺧﻄﺎ_در_ﺛﺒﺖ_اﻃﻼﻋﺎت = 416,
            ﺷﻨﺎﺳﻪ_ﭘﺮداﺧﺖ_ﻛﻨﻨﺪه_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 417,
            اﺷﻜﺎل_در_ﺗﻌﺮﻳﻒ_اﻃﻼﻋﺎت_ﻣﺸﺘﺮي = 418,
            ﺗﻌﺪاد_دﻓﻌﺎت_ورود_اﻃﻼﻋﺎت_از_ﺣﺪ_ﻣﺠﺎز_ﮔﺬﺷﺘﻪ_اﺳﺖ = 419,
            IP_نامعتبر_است = 421,

            ﺗﺮاﻛﻨﺶ_ﺗﻜﺮاري_اﺳﺖ = 51,
            ﺗﺮاﻛﻨﺶ_ﻣﺮﺟﻊ_ﻣﻮﺟﻮد_ﻧﻴﺴﺖ = 54,
            ﺗﺮاﻛﻨﺶ_ﻧﺎﻣﻌﺘﺒﺮ_اﺳﺖ = 55,
            ﺧﻄﺎ_در_واریز = 61
        }

        public Behardakht()
        {
            terminalId = Options.Payment.Behpardakht.terminalId;
            userName = Options.Payment.Behpardakht.userName;
            userPassword = Options.Payment.Behpardakht.userPassword;
            CallbaclUrl = string.Format(Options.Payment.Global.CallbackUrl, "Behardakht");//   "http://pmkala.ir/Payment/Callback?gateway=Behardakht";
        }
        public override PaymentForm CreateForm(ref T t)
        {
            var f = new PaymentForm();
            f.ActionUrl = "https://pgw.bpm.bankmellat.ir/pgwchannel/startpay.mellat";
            f.Method = PaymentForm.MethodType.Post;
            f.Data.Add("RefId", t.Token);
            return f;
        }

        public override void InitTransaction(ref T t)
        {

            var persianCalendar = new PersianCalendar();
            int y = persianCalendar.GetYear(DateTime.Now);
            int m = persianCalendar.GetMonth(DateTime.Now);
            int d = persianCalendar.GetDayOfMonth(DateTime.Now);

            string localDate = string.Format("{0:00}{1:00}{2:00}", y, m, d);
            string localTime = DateTime.Now.ToString("hhmmss"); ;

            var bm = new PaymentGatewayImplService();
            var res = bm.bpPayRequest(terminalId,
                        userName,
                        userPassword,
                        t.TransactionID,
                        t.Amount,
                        localDate,
                        localTime,
                        t.Comment,
                        CallbaclUrl,
                        0);

            int resCode;
            string refId = "";
            if (res.Contains(","))
            {
                resCode = res.Split(',')[0].ToInt(-1);
                refId = res.Split(',')[1];
            }
            else
            {
                resCode = res.ToInt(-1);
            }

            if (resCode == 0)
            {
                t.Token = refId;
                t.Status = TransactionStatus.Saved;
            }
            else
            {
                t.ErrorCode = resCode;
                t.Comment = GetMessage(resCode);
                t.Status = TransactionStatus.Unsuccessfull;
            }
        }

        public override bool VerfyTransaction(ref T tran)
        {
            var bm = new PaymentGatewayImplService();
            var RefId = HttpContext.Current.Request.Form["RefId"];
            var ResCode = HttpContext.Current.Request.Form["ResCode"].ToInt() ?? -1;
            var SaleOrderId = HttpContext.Current.Request.Form["SaleOrderId"].ToLong() ?? -1;
            var SaleReferenceId = HttpContext.Current.Request.Form["SaleReferenceId"].ToLong() ?? 0;


            if (ResCode != SUCCESS)
            {
                tran = null;
                return false;
            }

            Message = GetMessage(ResCode);

            //var ctx = new Transaction();// Daniz.Models.DanizContext();
            //tran = ctx.Transactions.Where(t => t.TransactionID == SaleOrderId).FirstOrDefault();
            //TODO tran = TransactionSelector(SaleOrderId);

            if (tran == null)
                return false;

            if (tran.Status != TransactionStatus.Saved)
            {
                tran.Comment = "این تراکنش قبلاً پردازش شده";
                return false;
            }

            try
            {
                var resultCode_bpVerifyRequest = bm.bpVerifyRequest(terminalId, userName, userPassword, tran.TransactionID,
                    SaleOrderId, SaleReferenceId);

                if (string.IsNullOrEmpty(resultCode_bpVerifyRequest))
                {
                    var resultCode_bpinquiryRequest = bm.bpInquiryRequest(terminalId, userName, userPassword, SaleOrderId, SaleOrderId, SaleReferenceId).ToInt(-1);

                    if (resultCode_bpinquiryRequest != SUCCESS)
                    {
                        //success
                        tran.Comment = Message = GetMessage(resultCode_bpinquiryRequest);
                        return false;
                    }
                    //return false;
                }
                else
                {
                    if (resultCode_bpVerifyRequest.ToInt(-1) != SUCCESS)
                        return false;
                }

                var resultCode_bpSettleRequest = bm.bpSettleRequest(terminalId, userName, userPassword, tran.TransactionID, SaleOrderId, SaleReferenceId).ToInt(-1);

                if (resultCode_bpSettleRequest != SUCCESS
                    && resultCode_bpSettleRequest != (int)MellatBankReturnCode.ﺗﺮاﻛﻨﺶ_Settle_ﺷﺪه_اﺳﺖ)
                {
                    Message = GetMessage(resultCode_bpSettleRequest);
                    return false;
                }

                tran.Status = TransactionStatus.Successfull;
                //ctx.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                tran.Comment = ex.Message;
                return false;
            }
        }

        public override int GetTransactionID()
        {
            return HttpContext.Current.Request.Form["SaleOrderId"].ToInt(-1);
        }

        public string GetMessage(int n)
        {
            switch (n)
            {
                case 0:
                    return "تراكنش با موفقيت انجام شد";
                case 11:
                    return "شماره كارت نامعتبر است";
                case 12:
                    return "موجودي كافي نيست";
                case 13:
                    return "رمز نادرست است";
                case 14:
                    return "تعداد دفعات وارد كردن رمز بيش از حد مجاز است";
                case 15:
                    return "كارت نامعتبر است";
                case 16:
                    return "دفعات برداشت وجه بيش از حد مجاز است";
                case 17:
                    return "كاربر از انجام تراكنش منصرف شده است";
                case 18:
                    return "تاريخ انقضاي كارت گذشته است";
                case 19:
                    return "مبلغ برداشت وجه بيش از حد مجاز است";
                case 111:
                    return "صادر كننده كارت نامعتبر است";
                case 112:
                    return "خطاي سوييچ صادر كننده كارت";
                case 113:
                    return "پاسخي از صادر كننده كارت دريافت نشد";
                case 114:
                    return "دارنده كارت مجاز به انجام اين تراكنش نيست";
                case 21:
                    return "پذيرنده نامعتبر است";
                case 23:
                    return "خطاي امنيتي رخ داده است";
                case 24:
                    return "اطلاعات كاربري پذيرنده نامعتبر است";
                case 25:
                    return "مبلغ نامعتبر است";
                case 31:
                    return "پاسخ نامعتبر است";
                case 32:
                    return "فرمت اطلاعات وارد شده صحيح نمي باشد";
                case 33:
                    return "حساب نامعتبر است";
                case 34:
                    return "خطاي سيستمي";
                case 35:
                    return "تاريخ نامعتبر است";
                case 41:
                    return "شماره درخواست تكراري است";
                case 42:
                    return "تراكنش  Saleيافت نشد";
                case 43:
                    return "قبلا درخواست  Verifyداده شده است";
                case 44:
                    return "درخواست  Verfiyيافت نشد";
                case 45:
                    return "تراكنش  Settleشده است";
                case 46:
                    return "تراكنش  Settleنشده است";
                case 47:
                    return "تراكنش  Settleيافت نشد";
                case 48:
                    return "تراكنش  Reverseشده است";
                case 49:
                    return "تراكنش  Refundيافت نشد";
                case 412:
                    return "شناسه قبض نادرست است";
                case 413:
                    return "شناسه پرداخت نادرست است";
                case 414:
                    return "سازمان صادر كننده قبض نامعتبر است";
                case 415:
                    return "زمان جلسه كاري به پايان رسيده است";
                case 416:
                    return "خطا در ثبت اطلاعات";
                case 417:
                    return "شناسه پرداخت كننده نامعتبر است";
                case 418:
                    return "اشكال در تعريف اطلاعات مشتري";
                case 419:
                    return "تعداد دفعات ورود اطلاعات از حد مجاز گذشته است";
                case 421:
                    return "IPنامعتبر است";
                case 51:
                    return "تراكنش تكراري است";
                case 54:
                    return "تراكنش مرجع موجود نيست";
                case 55:
                    return "تراكنش نامعتبر است";
                case 61:
                    return "خطا در واريز";
                default:
                    return "";
            }
        }
    }
}