﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Tooska.Payment
{
    public class Pasargad<T> : AbstractPaymentGateway<T> where T : AbstractTransaction
    {
        private static int ACTION_SELL = 1003;
        private static int ACTION_REFUND = 1004;

        public string privateKey { get; set; }

        public int merchantCode { get; set; }

        internal bool test()
        {

            var merchantCode = 3648587;
            var terminalCode = 1397865;
            var invoiceNumber = 1;
            var invoiceDate = "2017/01/01 12:01:01";
            var amount = "1000";
            var redirectAddress = "http://pep.co.ir/ipgtest/";
            var action = 1003;
            var timeStamp = "2017/11/26 12:32:04";
            var sign = "ZQ7C8JmcDgsgqOF4+tOBDaPQodVQNYJSt/8Iw+tPlQ9zTjklfjo9h7bnCfsTdQB0syA0kdLUOityOIxjsCXjLCgA2CBfuMhJ9mV5XtMF0i5AxI5etcx7PmmNna0Bk0QVTpErzM7DWU5oAMnID804DNkb0DNaD2oKe3Kxh43ESjU=";
            var pk = "<RSAKeyValue><Modulus>g/67VnbOC78sCkIOxqFGWbQmi8Blk4Iyn4ew3XH7z/wRTCgL8JqH3quKvawKbzUW6RYSog6TFaZezVRfYIMXkCDRCesROWv2fwFZXvpk5Qvfz2gftd+wwfdbMg4gzqfI3mO4HYoXiN4UaB2V7lBJNKnddKL+vVp8v+goGjgqDu0=</Modulus><Exponent>AQAB</Exponent><P>uAS78/aCz7VjaJdfuBiVWQ8gZ1De/muXjbUqmRemOFOpMPDg6VauPtynNKVN2DiG+rbMtYZsFSDq6HYcU2KwEw==</P><Q>t6B3q6qHdcQQbCb0zpHr0tLxSzEYAoHXLA6vcapGqWg65VZ2qM/LAo4ws4czjHH/1BXWXFtA3W0BT/diPr4k/w==</Q><DP>isxHWERfdnvCd2l18U3ZmEsojcUZ7Z0JuwEjGEYWQfYYb6nAez++PfwAwlzM0oIQjZrD3Ud+zpD9nkk4JE0TsQ==</DP><DQ>Rsf2GRfcq0puQiIqD040Jsnk9OTxqlW+SV9HxXqrTjW9LhXMG3oteMiygSFGf4aZ5Hiebm9Ga5PPA7IKDsFgpQ==</DQ><InverseQ>NbvqiqAKlLtDXU2Hxta8DOuXNoPmX/+Agcf47aw5fyb6pc7JAOaJGKUYFe5cqP11njINWJh4HtOoJQJxFiOsSg==</InverseQ><D>cX5zWIo46tLLLFrWV6ZOVk1Xo2ygKnCV1KQYyJ/8lD46JVB9y7kjwKuhVSt7oVl1xJ9tEBkWEVSHSWgVxmeRtujSN6WP5Yrjk9sbyKXvi3cKt7dU1sy2CHn6CvyC9gCUIhi6Mg7aIMFk7xFt//Cs9PJbCMcMxZGIQk5kVvcyEaU=</D></RSAKeyValue>";

            SHA1CryptoServiceProvider hash = new SHA1CryptoServiceProvider();

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(pk);
            //              #merchantCode#terminalCode#invoiceNumber#invoiceDate#amount#redirectAddress#action#timeStamp#
            string data = $"#{merchantCode}#{terminalCode}#{invoiceNumber}#{invoiceDate}#{amount}#{redirectAddress}#{ACTION_SELL}#{timeStamp}#";

            byte[] signMain = rsa.SignData(Encoding.ASCII.GetBytes(data), SHA1CryptoServiceProvider.Create());
            var s1 = Convert.ToBase64String(signMain);

            return sign == s1;
        }

        public int terminalCode { get; set; }

        public Pasargad() : base()
        {
            privateKey = Options.Payment.Pasargad.privateKey;
            merchantCode = Options.Payment.Pasargad.merchantCode;
            terminalCode = Options.Payment.Pasargad.terminalCode;
        }
        public override PaymentForm CreateForm(ref T t)
        {
            var f = new PaymentForm();
            f.Method = PaymentForm.MethodType.Post;
            f.ActionUrl = "https://pep.shaparak.ir/gateway.aspx";
            f.Data.Add("merchantCode", merchantCode);
            f.Data.Add("terminalCode", terminalCode);
            f.Data.Add("invoiceNumber", t.TransactionID);
            f.Data.Add("invoiceDate", t.Data2);
            f.Data.Add("amount", t.Amount);
            f.Data.Add("redirectAddress", CallbaclUrl);
            f.Data.Add("action", ACTION_SELL);
            f.Data.Add("timeStamp", t.Data2);

            f.Data.Add("sign", t.Data1);
            return f;
        }

        public override int GetTransactionID()
        {
            return HttpContext.Current.Request.QueryString["iN"].ToInt() ?? -1;
        }

        public override void InitTransaction(ref T t)
        {
            t.CreateDate = DateTime.Now;
            t.Data2 = t.CreateDate.ToString("yyyy/MM/dd HH:mm:ss");
            t.Data1 = Encode(t);
        }

        public override bool VerfyTransaction(ref T t)
        {
            string invoiceNumber = HttpContext.Current.Request.QueryString["iN"]; // شماره فاکتور
            string invoiceDate = HttpContext.Current.Request.QueryString["iD"]; // تاریخ فاکتور
            string TransactionReferenceID = HttpContext.Current.Request.QueryString["tref"]; // شماره مرجع

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://pep.shaparak.ir/CheckTransactionResult.aspx");
            string text = "invoiceUID=" + HttpContext.Current.Request.QueryString["tref"];
            byte[] textArray = Encoding.UTF8.GetBytes(text);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = textArray.Length;
            request.GetRequestStream().Write(textArray, 0, textArray.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            string result = reader.ReadToEnd();
            return true;
        }

        private string Encode(T t)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(privateKey);
            //              #merchantCode#terminalCode#invoiceNumber#invoiceDate#amount#redirectAddress#action#timeStamp#
            string data = $"#{merchantCode}#{terminalCode}#{t.TransactionID}#{t.Data2}#{t.Amount}#{CallbaclUrl}#{ACTION_SELL}#{t.Data2}#";

            byte[] signMain = rsa.SignData(Encoding.UTF8.GetBytes(data), new SHA1CryptoServiceProvider());
            return Convert.ToBase64String(signMain); 
        }
    }
}