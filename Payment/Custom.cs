﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tooska.Payment
{
    public class CustomTransaction<T> : AbstractPaymentGateway<T> where T : AbstractTransaction
    {
        public override PaymentForm CreateForm(ref T t)
        {
            var f = new PaymentForm();
            f.Method = PaymentForm.MethodType.Get;
            f.ActionUrl = Options.Payment.Global.DashboardUrl;
            f.ButtonText = "بازگشت به صقحه خانگی";
            return f;
        }

        public override int GetTransactionID()
        {
            throw new NotImplementedException();
        }

        public override void InitTransaction(ref T t)
        {
            t.Status = TransactionStatus.Successfull;
        }

        public override bool VerfyTransaction(ref T t)
        {
            throw new NotImplementedException();
        }

    }
    [Obsolete("InPlace has been obsoleted, use CustomTransaction")]
    public class InPlace<T> : CustomTransaction<T> where T : AbstractTransaction
    {

    }
}