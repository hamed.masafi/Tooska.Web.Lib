﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace Tooska.Payment
{
    public class Pay<T> : AbstractPaymentGateway<T> where T : AbstractTransaction
    {
        public class JsonParameters
        {
            public int status { get; set; }
            public string transId { get; set; }
            public double amount { get; set; }
            public string errorCode { get; set; }
            public string errorMessage { get; set; }
            public JsonParameters()
            {

            }
        }

        private string GatewaySend = "https://pay.ir/payment/send";
        private string GatewayResult = "https://pay.ir/payment/verify";

        public string Api { get; private set; }

        public Pay() : base()
        {
            Api = Options.Payment.Pay.Api;
        }

        public override PaymentForm CreateForm(ref T t)
        {
            var form = new PaymentForm();
            form.Method = PaymentForm.MethodType.Get;
            form.ActionUrl = "https://pay.ir/payment/gateway/" + t.Token;
            return form;
        }

        public override int GetTransactionID()
        {
            throw new NotImplementedException();
        }

        public override void InitTransaction(ref T t)
        {
            string result = "";
            String post_string = "";
            Dictionary<string, string> post_values = new Dictionary<string, string>();
            post_values.Add("api", Api);
            post_values.Add("amount", t.Amount.ToString());
            post_values.Add("redirect", CallbaclUrl);

            foreach (KeyValuePair<string, string> post_value in post_values)
            {
                post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
            }
            post_string = post_string.TrimEnd('&');
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(GatewaySend);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                result = responseStream.ReadToEnd();
                responseStream.Close();
            }
            
            JsonParameters Parmeters = JsonConvert.DeserializeObject<JsonParameters>(result);

            if (Parmeters.status == 1)
            {
                t.Token = Parmeters.transId;
                t.Status = TransactionStatus.Inited;
            }
            else
            {
                t.ErrorCode = Parmeters.errorCode.ToInt(0);
                Message = Parmeters.errorMessage;
                t.Status = TransactionStatus.Unsuccessfull;
            }
        }

        public override bool VerfyTransaction(ref T t)
        {
            if (t.Status != TransactionStatus.Inited)
            {
                Message = "تراکنش قبلاً پردازش شده است";
                return false;
            }

            string TransID = HttpContext.Current.Request.Form["transId"].ToString();
            string result = "";
            String post_string = "";
            Dictionary<string, string> post_values = new Dictionary<string, string>();
            post_values.Add("api", Api);
            post_values.Add("transId", TransID);

            foreach (KeyValuePair<string, string> post_value in post_values)
            {
                post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
            }
            post_string = post_string.TrimEnd('&');
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(GatewayResult);
            objRequest.Method = "POST";
            objRequest.ContentLength = post_string.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(post_string);
            myWriter.Close();

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                result = responseStream.ReadToEnd();
                responseStream.Close();
            }

            JsonParameters Parmeters = JsonConvert.DeserializeObject<JsonParameters>(result);

            if (Parmeters.status == 1)
            {
                t.Status = TransactionStatus.Successfull;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}