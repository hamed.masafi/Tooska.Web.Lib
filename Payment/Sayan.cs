﻿using Tooska.Payment.ir.shaparak.sayan;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

// https://sayan.shaparak.ir/ws/payment/merchant.wsdl
namespace Tooska.Payment
{

    public static class SayanTransactionExtensions
    {
        public static string PaymentTicket(this AbstractTransaction t)
        {
            return t.Token;
        }

        public static void PaymentTicket(this AbstractTransaction t, string value)
        {
            t.Token = value;
        }
    }
    public class Sayan<T> : AbstractPaymentGateway<T> where T : AbstractTransaction
    {

        //const string wsp1 = @"y1d1h0w1e7w9w3p9q8b6a1w3r2g9g1z5f5n9k2b5p6m5w0h7k9";
        //const string wsp2 = @"c8t2d0k3a8g0f1f7r6d5k7x9u0d9v5s9b9b4z0z8d7b3e8q0t0";
        //const string customerId = "pmkala";
        //const string merchantId = "A060";

        public string wsp1 { get; set; }
        public string wsp2 { get; set; }
        public string customerId { get; set; }
        public string merchantId { get; set; }


        public Sayan() : base()
        {
            wsp1 = Options.Payment.Sayan.wsp1;
            wsp2 = Options.Payment.Sayan.wsp2;
            customerId = Options.Payment.Sayan.customerId;
            merchantId = Options.Payment.Sayan.merchantId;
            CallbaclUrl = string.Format(Options.Payment.Global.CallbackUrl, "Sayan");
        }
        public override void InitTransaction(ref T t)
        {
            if (t.Status != TransactionStatus.Saved)
                return;

            var merchant = new merchant();
            var param = new getPaymentTicketRequest();
            param.wsp1 = wsp1;
            param.wsp2 = wsp2;
            param.merchantId = merchantId;
            param.customerId = "1";
            param.amount = t.Amount.ToString();
            param.paymentId = t.TransactionID.ToString();

            getPaymentTicketResponse res;
            try
            {
                res = merchant.getPaymentTicket(param);
            }
            catch (Exception ex)
            {
                res = new getPaymentTicketResponse()
                {
                    errorOccur = new errorOccur()
                    {
                        errorCode = -1,
                        errorDescription = ex.Message
                    }
                };
            }

            t.CreateDate = DateTime.Now;
            if (res.paymentTicket != null || res.errorOccur.errorCode == 0)
            {
                t.PaymentTicket(res.paymentTicket);
                t.Status = TransactionStatus.Inited;
            }
            else
            {
                t.Status = TransactionStatus.Unsuccessfull;
                t.ErrorCode = res.errorOccur.errorCode;
                t.Comment = res.errorOccur.errorDescription;
            }
        }

        public override PaymentForm CreateForm(ref T t)
        {
            var form = new PaymentForm();
            //paymentTicket
            //revertURL

            form.ActionUrl = "https://sayan.shaparak.ir/epay/info";
            form.Data.Add("paymentTicket", t.PaymentTicket());
            form.Data.Add("revertURL", CallbaclUrl);

            return form;
        }

        public override bool VerfyTransaction(ref T tran)
        {
            /*
                - resultCode :کد پاسخ که نشادهنده نتيجه عمليات می باشد. اين کد در صورت موفق بودن
                تراکنش مقدار 11 را دارد و در غير اينصورت نشاندهنده ناموفق بودن تراکنش بوده که طبق
                جدول مربوطه در پيوست2 اين مستند به پذيرندگان ارسال خواهد شد.
                - resultDetail :شامل توضيحات مربوط به نتيجه تراکنش به زبان فارسی می باشد.
                - paymentId :شناسه پرداخت تراکنش که در زمان دريافت paymentTicket از سوی
                سامانه پذيرنده توليد و قبال ارسال شده است. اين عنصر جهت مشخص شدن تراکنش توسط
                پذيرنده در هنگام بازگشت به سايت پذيرنده قابل استفاده است.
                - referenceNumber :شماره تراکنش درگاه می باشد که به صورت يکتا برای تراکنش توليد
                و به سامانه پذيرنده بازخواهد گشت. در مراحل بعدی اين عنصر اطالعاتی مورد کاربرد دارد. در
                صورت ارسال از سوی درگاه پرداخت، نمايش آن به دارنده کارت در سامانه پذيرنده الزامی
                است.
                - RRN :شماره ارجاع تراکنش می باشد که توسط سامانه شاپرک به تراکنش اختصاص می يايد
                و در صورت ارسال از سوی درگاه پرداخت، نمايش آن به دارنده کارت در سامانه پذيرنده الزامی
                است.
                راهنماي فني پذيرندگان درگاه پرداخت اينترنتي سايان كارت
                12
                - trace :شماره پيگيری تراکنش می باشد و و در صورت ارسال از سوی درگاه پرداخت ، نمايش
                آن به دارنده کارت در سامانه پذيرنده الزامی است.
                - amount :مبلغ تراکنش به ريال که نشاندهنده مبلغی تراکنش انجام شده در سامانه درگاه
                پرداخت می باشد.
             */

            var resultCode = HttpContext.Current.Request.Form["resultCode"].ToInt(-1);
            var resultDetail = HttpContext.Current.Request.Form["resultDetail"];
            var paymentId = HttpContext.Current.Request.Form["paymentId"].ToInt(0);
            var referenceNumber = HttpContext.Current.Request.Form["referenceNumber"];
            var RRN = HttpContext.Current.Request.Form["RRN"];
            var trace = HttpContext.Current.Request.Form["trace"];
            var amount = HttpContext.Current.Request.Form["amount"].ToInt(0);

            //var ctx = new Daniz.Models.DanizContext();
            //tran = ctx.Transactions.Where(t => t.TransactionID == paymentId).FirstOrDefault();
            //TODO: tran = TransactionSelector(paymentId);

            Message = GetCallbackMessage(resultCode);
            //بررسی وجود تراکنش
            if (tran == null)
            {
                Message = "تراکنش یافت نشد";
                return false;
            }

            //بررسی صحت کد
            if (resultCode != 0)
            {
                tran.Status = TransactionStatus.Unsuccessfull;
                Message = GetVerfyMessage(resultCode);
                return false;
            }

            //بررسی مبلغ
            if (tran.Amount != amount)
            {
                Message = "مبلغ تراکنش نادرست است";
                return false;
            }

            if (tran.Status != TransactionStatus.Inited)
            {
                Message = "تراکنش قبلاً پردازش شده است";
                return false;
            }


            var merchant = new merchant();
            var param = new verifyRequest();
            param.wsp1 = wsp1;
            param.wsp2 = wsp2;
            param.merchantId = merchantId;
            param.paymentTicket = tran.PaymentTicket();
            var res = merchant.verify(param);

            if (res.paymentId.ToInt(0) != tran.TransactionID)
            {
                Message = "خطا در تائید پرداخت";
                return false;
            }

            //if (res.resultCode.ToInt(-1) == 0)
            //{
            tran.Status = TransactionStatus.Successfull;
            //ctx.SaveChanges();
            return true;
            //}
            //else
            //{
            //    tran.Status = TransactionStatus.Unsuccessfull;
            //    Message = GetVerfyMessage(res.resultCode.ToInt(-1));
            //    return false;
            //}
        }

        public override int GetTransactionID()
        {
            return HttpContext.Current.Request.Form["paymentId"].ToInt(0);
        }

        public string GetVerfyMessage(int n)
        {
            switch (n)
            {
                case -21: return "وجود کاراکترهای غير مجاز در درخواست";
                case -30: return "تراکنش قبال برگشت خورده است";
                case -50: return "طول رشته درخواست غير مجاز است";
                case -51: return "خطا در درخواست";
                case -80: return "تراکنش مورد نظر يافت نشد";
                case -81: return "خطای داخلی بانک";
                case -90: return "تراکنش قبال تاييد شده است";
                default: return "خطای ناشناخته";
            }
        }

        public string GetCallbackMessage(int n)
        {
            switch (n)
            {
                case 0: return "تراکنش با موفقيت انجام شده است";
                case 01: return "صادر کننده کارت از انجام تراکنش صرفنظر کرد";
                case 02: return "عمليات تاييديه اين تراکنش قبال با موفقيت صورت پذيرفته است";
                case 03: return "پذيرنده فروشگاهی نامعتبر می باشد";
                case 04: return "کارت توسط دستگاه ضبط شود";
                case 05: return "از انجام تراکنش صرفنظر شد";
                case 06: return "بروز خطا";
                case 07: return "بدليل شرايط خاص کارت توسط دستگاه ضبط می شود";
                case 08: return "با تشخيص هويت دارنده کارت تراکنش موفق می باشد";
                case 09: return "سيستم مشغول است ، بعدا تراکنش ارسال نمائيد";
                case 10: return "عدم دريافت قسمتي از پيغام";
                case 12: return "تراکنش نامعتبر است";
                case 13: return "مبلغ تراکنش اصالحيه نادرست است";
                case 14: return "شماره کارت ارسالی نامعتبر است )وجود ندارد(";
                case 15: return "صادر کننده کارت نامعتبر است )وجود ندارد(";
                case 16: return "تراکنش مورد تاييد است";
                case 17: return "از انجام تراکنش صرفنظر شد";
                case 19: return "تراکنش مجددا ارسال شود";
                case 20: return "کد پاسخ نامعتبر است";
                case 21: return "هيچ عملی انجام نميگيرد";
                case 22: return "عملکرد نادرست (Malfunction System( سيستم";
                case 23: return "کارمزد ارسالی پذيرنده غير قابل قبول است";
                case 25: return "تراکنش اصلی يافت نشد";
                case 30: return "قالب پيام دارای اشکال است";
                case 31: return "پذيرنده توسط سوييچ پشتيبانی نمی شود";
                case 32: return "وجه بصورت کامل به مشتری پرداخت نشده، تراکنش اصالحيه آن صادر شده است";
                case 33: return "تاريخ انقضای کارت سپری شده است";
                case 34: return "کارت مظنون به تقلب است";
                case 36: return "کارت محدود شده است";
                case 38: return "تعداد دفعات ورود رمز غلط بيش از حد مجاز است";
                case 39: return "کارت حساب اعتباری ندارد";
                case 40: return "عمليات درخواستی پشتيبانی نمی گردد";
                case 41: return "کارت مفقودی می باشد";
                case 42: return "کارت حساب عمومی ندارد";
                case 43: return "کارت مسروقه می باشد";
                case 44: return "کارت حساب سرمايه گذاری ندارد";
                case 48: return "کد پاسخ نامعتبر است";
                case 51: return "موجودی کافی نمی باشد";
                case 52: return "کارت حساب جاری ندارد";
                case 53: return "کارت حساب قرض الحسنه ندارد";
                case 54: return "تاريخ انقضای کارت به پايان رسيده است";
                case 55: return "رمز کارت نامعتبر است";
                case 56: return "کارت نامعتبر";
                case 57: return "انجام تراکنش مربوطه توسط دارنده کارت مجاز نمی باشد";
                case 58: return "انجام تراکنش مربوطه توسط پايانه انجام دهنده مجاز نمی باشد";
                case 59: return "کارت مظنون به تقلب است";
                case 61: return "مبلغ تراکنش بيش از حد مجاز است";
                case 62: return "کارت محدود شده است";
                case 63: return "تمهيدات امنيتی رعايت نشده است";
                case 64: return "مبلغ تراکنش اصلی نامعتبر است";
                case 65: return "تعداد درخواست تراکنش بيش از حد مجاز است";
                case 66: return "شماره حساب فعال نيست";
                case 67: return "کارت توسط دستگاه ظبط شود";
                case 68: return "جواب دريافتی با تاخير آمده است";
                case 75: return "تعداد دفعات ورود رمز غلط بيش از حد مجاز است";
                case 76: return "از انجام تراکنش صرفنظر شد";
                case 77: return "روز مالی تراکنش نامعتبر است";
                case 78: return "کارت فعال نيست";
                case 79: return "حساب متصل به کارت نامعتبر يا دارای اشکال است";
                case 80: return "پردازش تراکنش دارای اشکال است";
                case 83: return "PSP با شاپرک SingOff نموده است";
                case 84: return "عدم دريافت پاسخ سوئيچ کارت صادر کننده";
                case 85: return "نامعتبر است Originator شماره نامعتبر است";
                case 86: return "شاپرک با بانک صادر کننده در حال Singoff";
                case 90: return "سامانه مقصد تراکنش در حال انجام عملیات پایان روز می باشد";
                case 91: return "عدم دريافت پاسخ سوئيچ صادر کننده";
                case 92: return "صادر کننده کارت نامعتبر است";
                case 93: return "تراکنش کامل نشده است";
                case 94: return "تراکنش تکراری است";
                case 96: return "از انجام تراکنش صرفنظر شد";
                case 97: return "فرآيند تغيير کليد برای صادر کننده يا پذيرنده در حال انجام است";
                case 130: return "صادرکننده کارت شناخته شده نمی باشد";
                case 150: return "خطای داخلی خطای داخلی خطای PSP";
                case 253: return "MERCHANT_NOT_FOUND";
                case 140: return "TIMEOUT TIMEOUT";
                case 254: return "PAYMENT_TICKET_IS_NOT_VALID";
                case 255: return "PAYMENT_TICKET_IS_REVOKED";
                case 256: return "PAYMENT_TICKET_IS_EXPIRED";

                case 110: return "تراکنش لغو شد";
                default: return "";
            }
        }

    }
}