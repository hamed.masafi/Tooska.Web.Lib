﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Tooska.Payment.com.zarinpal.www;

namespace Tooska.Payment
{
    public class Zarinpal<T> : AbstractPaymentGateway<T> where T : AbstractTransaction
    {
        [Display(Name = "شناسه پذیرنده")]
        public string merchantCode{ get; set; }

        public Zarinpal() : base()
        {
            merchantCode = Options.Payment.Zarinpal.marchantCode;
        }

        public override PaymentForm CreateForm(ref T t)
        {
            var form = new PaymentForm();
            form.ActionUrl = "https://www.zarinpal.com/pg/StartPay/";
            return form;
        }

        public override int GetTransactionID()
        {
            throw new NotImplementedException();
        }

        public override void InitTransaction(ref T t)
        {
            if (t.Status != TransactionStatus.Saved)
                return;

            System.Net.ServicePointManager.Expect100Continue = false;
            PaymentGatewayImplementationService zp = new PaymentGatewayImplementationService();
            string Authority;

            int Status = zp.PaymentRequest(merchantCode,
                (int)t.Amount / 10,
                t.Comment,
                "",
                "",
                "http://localhost/Verify.aspx",
                out Authority);

            if (Status == 100)
            {
                t.Token = Authority;
                t.Status = TransactionStatus.Saved;
                //Response.Redirect("https://www.zarinpal.com/pg/StartPay/" + Authority);
            }
            else
            {
                t.Status = TransactionStatus.Unsuccessfull;
                //Response.Write("error: " + Status);
            }
        }

        public override bool VerfyTransaction(ref T t)
        {
            var Authority = HttpContext.Current.Request.QueryString["Authority"];
            var Status = HttpContext.Current.Request.QueryString["Status"];

            if (string.IsNullOrEmpty(Authority) || string.IsNullOrEmpty(Status))
                return false;

            if (Status.Equals("OK"))
            {
                int Amount = 100;
                long RefID;
                System.Net.ServicePointManager.Expect100Continue = false;
                PaymentGatewayImplementationService zp = new PaymentGatewayImplementationService();

                int ServerStatus = zp.PaymentVerification(merchantCode,
                    Authority, Amount, out RefID);

                if (ServerStatus == 100)
                {
                    t.Status = TransactionStatus.Successfull;
                    //Response.Write("Success!! RefId: " + RefID);
                }
                else
                {
                    t.Status = TransactionStatus.Unsuccessfull;
                    //Response.Write("Error!! Status: " + Status);
                }
                return true;
            }
            else
            {
                //Response.Write("Error! Authority: " + Request.QueryString["Authority"].ToString() + " Status: " + Request.QueryString["Status"].ToString());
                return false;
            }
        }
    }
}