﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tooska.Payment
{
    public enum TransactionStatus
    {
        [Display(Name = "ثبت شده")]
        Saved,
        [Display(Name = "ارسال شده به بانک")]
        Inited,
        [Display(Name = "موفقیت آمیز")]
        Successfull,
        [Display(Name = "ناموفقیت آمیز")]
        Unsuccessfull
    }
    public enum TransactionGateway
    {
        [Display(Name = "پرداخت دستی")]
        CustomTransaction,
        [Display(Name = "بانک قوامین")]
        Sayan,
        [Display(Name = "بانک ملت")]
        Behardakht,

        [Display(Name = "زرین پال")]
        Zarinpal,

        [Display(Name = "پاسارگاه")]
        Pasargad,

        [Obsolete("InPlace has been obsoleted, use CustomTransaction")]
        [Display(Name = "پرداخت در محل")]
        InPlace = 99
    }

    public class AbstractTransaction
    {
        public AbstractTransaction()
        {
            Status = TransactionStatus.Saved;
        }

        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int TransactionID { get; set; }
        public int RelatedRowID { get; set; }

        //public string UserID { get; set; }
        //public IdentityUser User { get; set; }
        public long Amount { get; set; }
        public TransactionGateway? Gateway { get; set; }
        public TransactionStatus Status { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string UserComment { get; set; }

        //response from server
        public string Token { get; set; }
        public int ErrorCode { get; set; }
        public string Comment { get; set; }
        
        //extra data
        internal string Data1 { get; set; }
        internal string Data2 { get; set; }
        internal string Data3 { get; set; }
        internal string Data4 { get; set; }
    }

    public class AbstractTransaction<T> : AbstractTransaction where T : IdentityUser
    {
        public string UserID { get; set; }
        public T User { get; set; }
    }
}