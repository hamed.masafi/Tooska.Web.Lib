﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tooska.Attributes
{
    public class MaskAttribute : Attribute
    {
        public string Mask { get; set; }
        public MaskAttribute(string mask)
        {
            Mask = mask;
        }
    }

    [Obsolete("LongText os obsolated, use AllowHtml insetd")]
    public class LongTextAttribute : Attribute
    {
    }

    public class ColorAttribute : Attribute
    {
    }
    public class TooskaRequiredAttributeAdapter : RequiredAttributeAdapter
    {
        public TooskaRequiredAttributeAdapter(
            ModelMetadata metadata,
            ControllerContext context,
            RequiredAttribute attribute
        )
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                attribute.ErrorMessage = "مقدار {0} را وارد کنید.";
            }
        }
    }
    public class TooskaRegularExpressionAttributeAdapter : RegularExpressionAttributeAdapter
    {
        public TooskaRegularExpressionAttributeAdapter(
            ModelMetadata metadata,
            ControllerContext context,
            RegularExpressionAttribute attribute
        )
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                attribute.ErrorMessage = "اشتباه است.";
            }
        }
    }
    public class TooskaRangeAttributeAdapter : RangeAttributeAdapter
    {
        public TooskaRangeAttributeAdapter(
            ModelMetadata metadata,
            ControllerContext context,
            RangeAttribute attribute
        )
            : base(metadata, context, attribute)
        {
            if (attribute.ErrorMessage == null)
            {
                attribute.ErrorMessage = "اشتباه است.";
            }
        }

    }
    public class RequiredIfAttribute : RequiredAttribute
    {
        private String PropertyName { get; set; }
        private Object DesiredValue { get; set; }

        public RequiredIfAttribute(String propertyName, Object desiredvalue)
        {
            PropertyName = propertyName;
            DesiredValue = desiredvalue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            Object instance = context.ObjectInstance;
            Type type = instance.GetType();
            Object proprtyvalue = type.GetProperty(PropertyName).GetValue(instance, null);
            if (proprtyvalue.ToString() == DesiredValue.ToString())
            {
                ValidationResult result = base.IsValid(value, context);
                return result;
            }
            return ValidationResult.Success;
        }
    }
    public class RangeIfAttribute : RangeAttribute
    {
        private String PropertyName { get; set; }
        private Object DesiredValue { get; set; }

        public RangeIfAttribute(String propertyName, Object desiredvalue, double minimum, double maximum)
            : base(minimum, maximum)
        {
            PropertyName = propertyName;
            DesiredValue = desiredvalue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            Object instance = context.ObjectInstance;
            Type type = instance.GetType();
            Object proprtyvalue = type.GetProperty(PropertyName).GetValue(instance, null);
            if (proprtyvalue.ToString() == DesiredValue.ToString())
            {
                ValidationResult result = base.IsValid(value, context);
                return result;
            }
            return new ValidationResult("Not valid");//.Success;
        }
    }

    /*public class TooskaFileExtensionsAttribute : FileExtensionsAttribute
    {
       
    }*/
    public static class TooskaAttributes
    {
        public static void RegisterAttrs()
        {
            DataAnnotationsModelValidatorProvider.RegisterAdapter(
               typeof(RequiredAttribute),
               typeof(TooskaRequiredAttributeAdapter));

            DataAnnotationsModelValidatorProvider.RegisterAdapter(
                typeof(RegularExpressionAttribute),
                typeof(TooskaRegularExpressionAttributeAdapter));

            DataAnnotationsModelValidatorProvider.RegisterAdapter(
                typeof(RangeAttribute),
                typeof(TooskaRangeAttributeAdapter));

            DataAnnotationsModelValidatorProvider.RegisterAdapter(
                typeof(RequiredIfAttribute),
                typeof(RequiredAttributeAdapter));

            ClientDataTypeModelValidatorProvider.ResourceClassKey = "Messages";
            DefaultModelBinder.ResourceClassKey = "Messages";
        }
    }
}


