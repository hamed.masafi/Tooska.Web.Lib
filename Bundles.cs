﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tooska;
//using Yahoo.Yui.Compressor;

namespace Tooska
{
	public class Bundle
    {
        public Bundle()
        {
            Files = new string[] { };
            Requires = new string[] { };
            jQueryCode = null;
        }
        public string Name { get; set; }
        public string[] Files { get; set; }
        public string[] Requires { get; set; }

        public string jQueryCode { get; set; }
    }

    public class BundleCollection : Dictionary<string, Bundle>
    {
        public void Add(Bundle bundle)
        {
            Add(bundle.Name, bundle);
        }

        public static BundleCollection AlicationInstance
        {
            get
            {
                if (HttpContext.Current.Application["Tooska.BundleCollection.AlicationInstance"] == null)
                    return new BundleCollection();
                else
                    return (BundleCollection)HttpContext.Current.Application["Tooska.BundleCollection.AlicationInstance"];
            }
            set
            {
                HttpContext.Current.Application["Tooska.BundleCollection.AlicationInstance"] = value;
            }
        }

    }

    public static class BundleManager
    {
        // Not tested

        public static void ReadFolder(string folderPath)
        {
            ReadFolder(folderPath, true);
        }
        public static void ReadFolder(string folderPath, bool preferMinVersion)
        {
            if (folderPath.StartsWith("~"))
                folderPath = HttpContext.Current.Server.MapPath(folderPath);
            if (!System.IO.Directory.Exists(folderPath))
                throw new System.IO.DirectoryNotFoundException($"Directory {folderPath} not found");

            var bundle = Tooska.BundleCollection.AlicationInstance;

            //var imortedNames = GetNames();
            var folders = System.IO.Directory.GetDirectories(folderPath);

            foreach(var f in folders)
            {
                var name = System.IO.Path.GetFileName(f);

                if (bundle.ContainsKey(name))
                    continue;

                //var jsmin = new JavaScriptCompressor();
                //var cssmin = new CssCompressor();

                var allfiles = System.IO.Directory
                    .GetFiles(f, "*.*", System.IO.SearchOption.AllDirectories)
                    .Where(s => s.ToLower().EndsWith(".css") || s.ToLower().EndsWith(".js"))
                    .ToArray();

                var selectedFiles = new List<string>();
                foreach(var fileName in allfiles)
                {
                    var file = fileName.ToLower();
                    var ext = System.IO.Path.GetExtension(file);

                }
                var minFiles = allfiles
                    .Where(s => s.ToLower().Replace(".js", "").Replace(".css", "").EndsWith(".min"))
                    .ToList();

                var doubleModeFiles = minFiles
                    .Where(s => allfiles.Contains(s.Replace(".min", "")))
                    .ToList();

                var normalFiles = allfiles.ToList();
                //normalFiles.RemoveRange(doubleModeFiles);

                bundle.Add(new Bundle()
                {
                    Name = name,
                    Files = allfiles,
                    Requires = new[] { "jquery" }
                });
            }

            Tooska.BundleCollection.AlicationInstance = bundle;
        }
        public static void Register(string Name)
        {
            var imortedNames = GetNames();
            if (!imortedNames.Contains(Name))
                imortedNames.Add(Name);
            HttpContext.Current.Items["Tooska.BundleManager.RegisteredNames"] = imortedNames;
        }
        public static void Register(string[] Names)
        {
            var imortedNames = GetNames();
            imortedNames.AddRange(Names);
            HttpContext.Current.Items["Tooska.BundleManager.RegisteredNames"] = imortedNames;
        }

        //Not testes
        public static void InjectDependencies()
        {
            //var names = GetNames();
            //if (names.Count == 0)
            //    return;

            //var allBundles = BundleCollection.AlicationInstance;
            //var importedNames = new List<string>();

            //do
            //{
            //    var selectedBundles = allBundles.Where(b => b.Value.Requires.All(r => importedNames.Contains(r)))
            //        .ToList();

            //    if (selectedBundles.Count == 0)
            //        break;

            //    importedNames.AddRange(selectedBundles.Select(b => b.Key).ToList());
            //    importedNames.ForEach((s) => allBundles.Remove(s));
            //} while (true);

            //if (allBundles.Count > 0)
            //{
            //    var remainBundlesNames = string.Join(", ", allBundles.Select(b => b.Key).ToArray());
            //    throw new Exception($"Dependency for bundles {remainBundlesNames} not found.");
            //}

            //HttpContext.Current.Items.Remove("Tooska.BundleManager.RegisteredNames");
            //Register(importedNames.ToArray());
        }

        public static string Debug()
        {
            return string.Format("Names={0}<br/>All={1}",
                string.Join(",", GetNames()),
                string.Join(",", BundleCollection.AlicationInstance.Keys));
        }
        internal static List<string> GetNames()
        {
            if (HttpContext.Current.Items["Tooska.BundleManager.RegisteredNames"] == null)
                return new List<string>();
            else
                return (List<string>)HttpContext.Current.Items["Tooska.BundleManager.RegisteredNames"];
        }
        internal static List<Bundle> GetDependencies()
        {
            var imortedNames = GetNames();

            if (imortedNames.Count == 0)
                return new List<Bundle>();

            return BundleCollection.AlicationInstance
                .Where(d => imortedNames.Contains(d.Key))
                .Select(d => d.Value)
                .ToList();
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    class DependencyAttribute : ActionFilterAttribute, IActionFilter
    {
        string[] names = null;

        public DependencyAttribute()
        {

        }
        public DependencyAttribute(string Name)
        {
            names = new[] { Name };
        }
        public DependencyAttribute(params string[] Names)
        {
            names = Names;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (names != null)
                BundleManager.Register(names);
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
    }

}
