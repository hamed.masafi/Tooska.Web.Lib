﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace Tooska.Utility
{
    public struct UrlInfo
    {
        public string Title;
        public string Url;
        public int Order;
        public Icon Icon;
        public UrlInfo(string title, string url)
        {
            Title = title;
            Url = url;
            Order = 0;
            Icon = null;
        }

        public UrlInfo(string title, string url, int order)
        {
            Title = title;
            Url = url;
            Order = order;
            Icon = null;
        }

        public UrlInfo(string title, string url, Icon icon, int order)
        {
            Title = title;
            Url = url;
            Order = order;
            Icon = icon;
        }
    }

    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
    internal class SitemapAttribute : ActionFilterAttribute, IActionFilter
    {
        UrlInfo info;

        public SitemapAttribute()
        {
            info = new UrlInfo();
        }
        public SitemapAttribute(string Title)
        {
            info = new UrlInfo()
            {
                Title = Title
            };
        }

        public SitemapAttribute(string Title, int Order)
        {
            info = new UrlInfo()
            {
                Title = Title,
                Order = Order
            };
        }

        public SitemapAttribute(string Title, string Url, int Order)
        {
            info = new UrlInfo()
            {
                Title = Title,
                Url = Url,
                Order = Order
            };
        }

        public SitemapAttribute(string Title, string Url, int Order, FontAwesomeIcons Icon)
        {
            info = new UrlInfo()
            {
                Title = Title,
                Url = Url,
                Order = Order,
                Icon = Icon,
            };
        }

        public SitemapAttribute(string Title, string Url, int Order, Glyphicons Icon)
        {
            info = new UrlInfo()
            {
                Title = Title,
                Url = Url,
                Order = Order,
                Icon = Icon,
            };
        }

        public SitemapAttribute(string Title, string Url, int Order, SimpleIcons Icon)
        {
            info = new UrlInfo()
            {
                Title = Title,
                Url = Url,
                Order = Order,
                Icon = Icon,
            };
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            SiteMapManager.Add(info);// title, url, icon, order);
            base.OnActionExecuted(filterContext);
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }
    }

    public class SiteMapCollection : List<UrlInfo>
    {
        //public void Add(string title, string url)
        //{
        //    Add(new UrlInfo(title, url));
        //}

        //public void Insert(int index, string title, string url)
        //{
        //    Insert(index, new UrlInfo(title, url));
        //}
    }

    public class SiteMapManager
    {
        public static void Add(UrlInfo urlInfo)
        {
            var tmp = AlicationInstance;
            tmp.Add(urlInfo);
            AlicationInstance = tmp;
        }
        public static void Add(string title)
        {
            var tmp = AlicationInstance;
            tmp.Add(new UrlInfo(title, "", null, tmp.Count));
            AlicationInstance = tmp;
        }

        public static void Add(string title, int order)
        {
            var tmp = AlicationInstance;
            tmp.Add(new UrlInfo(title, "", order));
            AlicationInstance = tmp;
        }

        public static void Add(string title, string url)
        {
            var tmp = AlicationInstance;
            tmp.Add(new UrlInfo(title, url, tmp.Count));
            AlicationInstance = tmp;
        }

        public static void Add(string title, string url, int order)
        {
            var tmp = AlicationInstance;
            tmp.Add(new UrlInfo(title, url, order));
            AlicationInstance = tmp;
        }

        public static void Add(string title, string url, Icon icon, int order)
        {
            var tmp = AlicationInstance;
            tmp.Add(new UrlInfo(title, url, icon, order));
            AlicationInstance = tmp;
        }

        public static void Insert(int index, string title)
        {
            var tmp = AlicationInstance;
            tmp.Insert(index, new UrlInfo(title, "", index));
            AlicationInstance = tmp;
        }

        public static void Insert(int index, string title, string url)
        {
            var tmp = AlicationInstance;
            tmp.Insert(index, new UrlInfo(title, url, index));
            AlicationInstance = tmp;
        }

        public static SiteMapCollection AlicationInstance
        {
            get
            {
                if (HttpContext.Current.Items["Tooska.Utility.SiteMapManager"] == null)
                    return new SiteMapCollection();
                else
                    return (SiteMapCollection)HttpContext.Current.Items["Tooska.Utility.SiteMapManager"];
            }
            set
            {
                HttpContext.Current.Items["Tooska.Utility.SiteMapManager"] = value;
            }
        }

    }
}