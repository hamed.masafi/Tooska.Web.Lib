﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Tooska;
using Tooska.Attributes;

public static class TooskaExtensions
{

    public static string IconClassName(this object e)
    {
        if (e == null)
        {
            return "";
        }
        else
        {
            MemberInfo[] Info = e.GetType().GetMember(e.ToString());
            object[] Att = Info[0].GetCustomAttributes(typeof(IconAttribute), false);
            if (Att.Length == 0)
                return e.ToString();
            return ((IconAttribute)Att[0]).ClassName();
        }
    }

    public static string Display(this Enum e)
    {
        if (e == null)
        {
            return "";
        }
        else
        {
            MemberInfo[] Info = e.GetType().GetMember(e.ToString());
            object[] Att = Info[0].GetCustomAttributes(typeof(DisplayAttribute), false);
            if (Att.Length == 0)
                return e.ToString();
            return ((DisplayAttribute)Att[0]).Name;
        }
    }

    public static DateTime? FromJalali(string date)
    {
        if (string.IsNullOrWhiteSpace(date))
            return null;

        Regex r = new Regex(@"(\d{4})\/(\d{1,2})\/(\d{1,2})");
        var m = r.Match(date);

        if (m.Success)
        {

            int yy, mm, dd;
            if (int.TryParse(m.Groups[1].Value, out yy)
                && int.TryParse(m.Groups[2].Value, out mm)
                && int.TryParse(m.Groups[3].Value, out dd))
            {
                var persianCalendar = new PersianCalendar();
                return new DateTime(yy, mm, dd, persianCalendar);
            }
        }

        return null;
    }

    public static void FromJalali(this DateTime dateTime, string date)
    {
        var persianCalendar = new PersianCalendar();
        var dparts = date.Split('/');
        if (dparts.Length == 3)
        {
            int yy, mm, dd;
            if (int.TryParse(dparts[0], out yy)
                && int.TryParse(dparts[1], out mm)
                && int.TryParse(dparts[2], out dd))
            {
                dateTime = new DateTime(yy, mm, dd, persianCalendar);
            }
        }
    }

    public static string ToSafeString(this object s)
    {
        return s.ToSafeString("");
    }
    public static string ToSafeString(this object s, string defaultValue)
    {
        return s == null ? defaultValue : s.ToString();
    }
    public static string ToJalali(this DateTime dateTime)
    {
        if (dateTime == null)
            return null;

        if (dateTime.Year <= 622 || dateTime.Year >= 9999)
            return null;

        var persianCalendar = new PersianCalendar();
        int y = persianCalendar.GetYear(dateTime);
        int m = persianCalendar.GetMonth(dateTime);
        int d = persianCalendar.GetDayOfMonth(dateTime);

        return string.Format("{0}/{1}/{2} {3}:{4}:{5}",
            y, m, d,
            dateTime.Hour, dateTime.Minute, dateTime.Second);

    }
    public static string ToJalaliDate(this DateTime dateTime)
    {
        if (dateTime == null)
            return null;

        var persianCalendar = new PersianCalendar();
        int y = persianCalendar.GetYear(dateTime);
        int m = persianCalendar.GetMonth(dateTime);
        int d = persianCalendar.GetDayOfMonth(dateTime);

        return string.Format("{0}/{1}/{2}",
            y, m, d);
    }

    public static string ToCurrency(this int n)
    {
        if (n == 0)
            return "0";
        else
            return string.Format("{0:0,0}", n);
    }

    public static string ToDigitString(this decimal n)
    {
        return ((long)n).ToDigitString();
    }
    public static string ToDigitString(this int n)
    {
        return ((long)n).ToDigitString();
    }
    public static string ToDigitString(this long n)
    {
        var numbers1 = new string[] { string.Empty, "یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه" };
        var numbers2 = new string[] { "ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده" };
        var numbersTen = new string[] { "", "", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود" };
        var numbersHundred = new string[] { string.Empty, "یکصد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد" };
        var numbersThausand = new string[] { string.Empty, " هزار", " میلیون", " میلیارد", " تریلیون" };

        Func<long, string> translate3digit = (long group3) => {
            if (group3 == 0)
                return "";

            var y1 = group3 % 10;
            group3 /= 10;
            var y2 = group3 % 10;
            group3 /= 10;
            var y3 = group3 % 10;

            string ret = "";
            if (y2 == 1)
                ret = numbers2[y1];
            else if (y2 == 0)
                ret = numbers1[y1];
            else if (y1 == 0)
                ret = numbersTen[y2];
            else
                ret = numbersTen[y2] + " و " + numbers1[y1];

            if (y3 != 0)
            {
                if (ret == "")
                    ret = numbersHundred[y3];
                else
                    ret = numbersHundred[y3] + " و " + ret;
            }

            return ret;
        };


        string numberString = "";
        var index = 0;
        do
        {
            var group = n % 1000;
            var buffer = translate3digit(group);
            if (buffer != "")
            {
                if (index == 0)
                    numberString = buffer;
                else
                    numberString = buffer + " " + numbersThausand[index] + " و " + numberString;
            }
            if (numberString.EndsWith(" و "))
                numberString = numberString.Remove(numberString.Length - 3, 3);

            n /= 1000;
            index++;
        } while (n > 0);
        return numberString;
    }

    public static string RemoveFromStart(this string s, string Subject)
    {
        var clone = s;
        if (s.StartsWith(Subject))
            clone.Remove(0, Subject.Length);
        return clone;
    }

    public static string RemoveFromEnd(this string s, string Subject)
    {
        var clone = s;
        if (s.EndsWith(Subject))
            clone.Remove(s.Length - Subject.Length, Subject.Length);
        return clone;
    }

    public static bool IsNumeric(this string s)
    {
        return s.All(ch => char.IsNumber(ch));
    }

    public static int? ToInt(this string s)
    {
        if (s == null)
            return null;

        int n;
        if (int.TryParse(s, out n))
            return n;
        else
            return null;
    }
    public static int ToInt(this string s, int defaultValue)
    {
        return s.ToInt() ?? defaultValue;
    }

    //Deprased, use ?? operator instent
    [Obsolete]
    public static int ToInt(this int? n, int defaultValue)
    {
        return n ?? defaultValue;
    }
    public static long? ToLong(this string s)
    {
        if (s == null)
            return null;

        long n;
        if (long.TryParse(s, out n))
            return n;
        else
            return null;
    }
    [Obsolete]
    public static long ToLong(this string s, long defaultValue)
    {
        return s.ToLong() ?? defaultValue;
    }

    public static bool ToBool(this string s)
    {
        return s.ToBool(false);
    }
    public static bool ToBool(this string s, bool defaultValue)
    {
        if (s.ToLower() == bool.TrueString.ToLower())
            return true;

        if (s.ToLower() == bool.FalseString.ToLower())
            return false;

        bool n;
        if (bool.TryParse(s, out n))
            return n;
        else
            return defaultValue;
    }


    public static string Display(this DateTimeOffset dts)
    {
        return string.Format("{0} سال و {1} ماه و {2} روز", dts.Year, dts.Month, dts.Day);
    }

    public static void ThrowError(this Controller c, System.Net.HttpStatusCode code)
    {
        throw new HttpException((int)code, "");
    }

    public static DbGeography FromLocation(this DbGeography geo, double Longitude, double Latitude)
    {
        return DbGeography.FromText("POINT(" + Longitude + " " + Latitude + ")");
    }

    [Obsolete]
    public static void Message(this Controller c, ButtonType type, string message)
    {
        c.ViewBag.AlertMessage = message;
        c.ViewBag.AlertType = type.ToString().ToLower();
    }

    public static void Message(this Controller c, MessageType type, string message)
    {
        c.ViewBag.AlertMessage = message;
        c.ViewBag.AlertType = type.ToString().ToLower();
    }

    //public static MvcHtmlString EnumTemplate<T>(this HtmlHelper t, Type ty, string Template, params T[] excepts) where T : struct, IConvertible
    //{
    //    var values = Enum.GetValues(ty);
    //    string html = "";
    //    foreach (var v in values)
    //    {
    //        var en = (T)v;
    //        foreach (var e in excepts)
    //            if (en.Equals(e))
    //                continue;

    //        html += string.Format(Template,
    //            v,
    //            ((Enum)v).Display());
    //    }
    //    return MvcHtmlString.Create(html);
    //}
}

