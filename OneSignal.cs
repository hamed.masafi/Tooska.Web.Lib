﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace Tooska
{
    public class OneSignal
    {
        public string AppID { get; set; }
        public string RestKey { get; set; }

        struct Message
        {
            public string Title { get; set; }
            public string Body { get; set; }

            public Message(string title, string body)
            {
                Title = title;
                Body = body;
            }
        }
        Dictionary<string, Message> Messages = new Dictionary<string, Message>();

        public List<string> Users { get; set; }
        Dictionary<string, object> AdditionalData = new Dictionary<string, object>();
        public OneSignal()
        {
            Users = new List<string>();
        }
        public void AddUser(string UserID)
        {
            Users.Add(UserID);
        }
        public void AddMessage(string lang, string title, string body)
        {
            Messages[lang] = new Message(title, body);
        }
        public void AddData(string name, object value)
        {
            AdditionalData[name] = value;
        }
        //public string Title { get; set; }
        //public string Message { get; set; }
        //public object AdditionalData { get; set; }
        public string UserID { get; set; }
        public string Result { get; set; }
        public void Send()
        {
            var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            request.KeepAlive = true;
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";

            request.Headers.Add("authorization", "Basic ZTYzNzhjNDItNzllMC00ZWNjLWE5NmUtZmM4NTFiM2FlZTgw");

            var serializer = new JavaScriptSerializer();

            dynamic data = new JObject();
            JObject content = new JObject();
            JObject headings = new JObject();
            JObject extraData = new JObject();

            
            foreach (var msg in Messages)
            {
                headings.Add(msg.Key, msg.Value.Title);
                content.Add(msg.Key, msg.Value.Body);
            }
            //extraData.Add("extra", "info");
            //data.data = AdditionalData;
            if (AdditionalData.Count > 0)
            {
                JObject d = new JObject();
                foreach (var info in AdditionalData)
                    d.Add(info.Key, info.Value.ToString());
                data.data = d;
            }

            data.app_id = AppID;
            data.headings = headings;
            data.contents = content;
            

            if (Users.Count > 0)
            {
                JArray usersArray = new JArray();
                foreach (var u in Users)
                    usersArray.Add(u);
                data.include_player_ids = usersArray;
            }
            else
            {
                data.included_segments = new JArray("All");
            }
            //var c = Messages.Select(m => new { m.Key = m.Value.Body });
            //UserID = > include_player_ids
           /* var obj = new
            {
                app_id = AppID,
                contents = new { en = Title },
                //headings = new { en = Message },
                included_segments = new string[] { "All" },
                data = AdditionalData
            };
            var param = serializer.Serialize(obj);*/
            byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());

            string responseContent = null;

            try
            {
                using (var writer = request.GetRequestStream())
                {
                    writer.Write(byteArray, 0, byteArray.Length);
                }

                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        responseContent = reader.ReadToEnd();
                        Result = responseContent + "; " + data.ToString();
                        //{"id":"","recipients":0,"errors":["All included players are not subscribed"]}
                    }
                }
            }
            catch (WebException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
                Result = ex.Message + "; " + data.ToString();
            }
            
            System.Diagnostics.Debug.WriteLine(responseContent);
        }
    }
}