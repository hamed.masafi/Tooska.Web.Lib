﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.WebHost;
using System.Web.Mvc;

namespace Tooska.Mvc
{
    public class ApiController : System.Web.Http.ApiController
    {
        protected T GetParam<T>(string key)
        {
            var p = Request.Content.ReadAsAsync<JObject>();
            if (p.Result == null)
                return default(T);

            return (T)Convert.ChangeType(p.Result[key], typeof(T)); // example conversion, could be null...
        }

        [Obsolete("Use Success(...)")]
        protected Result<T> Result<T>(T data)
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;//((HttpControllerHandler)HttpContext.Current.Handler)
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;// HttpContext.Current.Request.QueryString["id"].ToInt() ?? 0;
            return new Result<T>(data, routeData.Values["action"].ToSafeString(), requestId);
        }

        [Obsolete("Use Error(...)")]
        protected Result<T> Result<T>(T data, int code)
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;
            var t = new Result<T>(code, "", routeData.Values["action"].ToSafeString(), requestId);
            t.SetError(code, "");
            return t;
        }

        protected Result Success()
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;//((HttpControllerHandler)HttpContext.Current.Handler)
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;// HttpContext.Current.Request.QueryString["id"].ToInt() ?? 0;
            return new Result(routeData.Values["action"].ToSafeString(), requestId);
        }

        protected Result<T> Success<T>(T data)
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;//((HttpControllerHandler)HttpContext.Current.Handler)
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;// HttpContext.Current.Request.QueryString["id"].ToInt() ?? 0;
            return new Result<T>(data, routeData.Values["action"].ToSafeString(), requestId);
        }

        protected Result<T> Success<T>(T data, int code)
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;
            var t = new Result<T>(code, "", routeData.Values["action"].ToSafeString(), requestId);
            t.SetError(code, "");
            return t;
        }

        protected Result Error(int code, string message)
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;//((HttpControllerHandler)HttpContext.Current.Handler)
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;// HttpContext.Current.Request.QueryString["id"].ToInt() ?? 0;
            return new Result(code, message, routeData.Values["action"].ToSafeString(), requestId);
        }

        protected Result<T> Error<T>(int code, string message)
        {
            var routeData = HttpContext.Current.Request.RequestContext.RouteData;//((HttpControllerHandler)HttpContext.Current.Handler)
            int requestId = routeData.Values["id"].ToSafeString().ToInt() ?? 0;// HttpContext.Current.Request.QueryString["id"].ToInt() ?? 0;
            return new Result<T>(code, message, routeData.Values["action"].ToSafeString(), requestId);
        }
    }
    public enum ResultType
    {
        Success,
        Error
    }
    public struct MetaData
    {
        public string Method;
        public string Params;
        public int RequestID;
    }
    public class Result
    {
        public ResultType Type { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }
        public MetaData Meta { get; set; }

        public Result(string methodName, int requestId)
        {
            Meta = new MetaData()
            {
                RequestID = requestId,
                Method = methodName
            };
            Type = ResultType.Success;
        }
        public void SetError(int code, string message)
        {
            Type = ResultType.Error;
            Code = code;
            Message = message;
        }
        public Result(int code, string message, string methodName, int requestId)
        {
            Meta = new MetaData()
            {
                RequestID = requestId,
                Method = methodName
            };
            Type = ResultType.Error;
            Code = code;
            Message = message;
        }
    }
    public class Result<T> : Result
    {
        public T Data { get; set; }

        public Result(T data, string methodName, int requestId) : base(methodName, requestId)
        {
            Data = data;
        }

        public Result(int code, string message, string methodName, int requestId) : base(methodName, requestId)
        {
            Meta = new MetaData()
            {
                RequestID = requestId,
                Method = methodName
            };
            Type = ResultType.Error;
            Code = code;
            Message = message;
        }

    }
}
