using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace Tooska
{

    public class Options
    {
        public class SiteMap
        {
            [Obsolete]
            public static string RootTitle
            {
                get
                {
                    return HttpContext.Current.Application["Tooska.SiteMap.RootTitle"].ToSafeString("");
                }
                set
                {
                    HttpContext.Current.Application["Tooska.SiteMap.RootTitle"] = value;
                }
            }

            public static Utility.UrlInfo? RootUrl
            {
                get
                {
                    if (HttpContext.Current.Application["Tooska.SiteMap.RootUrl"] == null)
                        return null;

                    return (Utility.UrlInfo)HttpContext.Current.Application["Tooska.SiteMap.RootUrl"];
                }
                set
                {
                    HttpContext.Current.Application["Tooska.SiteMap.RootUrl"] = value;
                }
            }
        }
        public class Payment
        {
            public class Global
            {
                public static string CallbackUrl
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Global.CallbackUrl"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Global.CallbackUrl"] = value;
                    }
                }
                public static string DashboardUrl
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Global.DashboardUrl"].ToSafeString("/");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Global.DashboardUrl"] = value;
                    }
                }
            }
            public class Sayan
            {
                public static string wsp1
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Sayan.wsp1"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Sayan.wsp1"] = value;
                    }
                }
                public static string wsp2
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Sayan.wsp2"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Sayan.wsp2"] = value;
                    }
                }
                public static string customerId
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Sayan.customerId"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Sayan.customerId"] = value;
                    }
                }
                public static string merchantId
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Sayan.merchantId"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Sayan.merchantId"] = value;
                    }
                }
            }

            public class Behpardakht
            {
                public static long terminalId
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Behpardakht.terminalId"].ToSafeString("0").ToLong() ?? 0;
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Behpardakht.terminalId"] = value;
                    }
                }
                public static string userName
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Behpardakht.userName"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Behpardakht.userName"] = value;
                    }
                }
                public static string userPassword
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Behpardakht.userPassword"].ToSafeString("");
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Behpardakht.userPassword"] = value;
                    }
                }
            }

            public class Zarinpal
            {
                public static string marchantCode
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Zarinpal.marchantCode"].ToSafeString();
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Zarinpal.marchantCode"] = value;
                    }
                }
            }

            public class Pay
            {
                public static string Api
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Pay.Api"].ToSafeString();
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Pay.Api"] = value;
                    }
                }
            }

            public class Pasargad
            {
                public static int merchantCode
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Pasargad.marchantCode"].ToSafeString().ToInt() ?? 0;
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Pasargad.marchantCode"] = value;
                    }
                }
                public static int terminalCode
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Pasargad.terminalCode"].ToSafeString().ToInt() ?? 0;
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Pasargad.terminalCode"] = value;
                    }
                }
                public static string privateKey
                {
                    get
                    {
                        return HttpContext.Current.Application["Tooska.Payment.Pasargad.privateKey"].ToSafeString();
                    }
                    set
                    {
                        HttpContext.Current.Application["Tooska.Payment.Pasargad.privateKey"] = value;
                    }
                }
            }
        }
    }

}