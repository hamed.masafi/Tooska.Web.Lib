﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tooska
{
    [Obsolete]
    public class Utilities
    {
        static readonly string[] SizeSuffixes =
                   { "bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

        static readonly string[] SizeSuffixesPersian =
                   { "بایت", "کیلوبایت", "مگابایت", "گیگابایت", "ترابایت", "PB", "EB", "ZB", "YB" };
        public static string SizeSuffix(Int64 value, int decimalPlaces = 1)
        {
            if (value < 0) { return "-" + SizeSuffix(-value); }
            if (value == 0) { return "0.0 بایت"; }

            // mag is 0 for bytes, 1 for KB, 2, for MB, etc.
            int mag = (int)Math.Log(value, 1024);

            // 1L << (mag * 10) == 2 ^ (10 * mag) 
            // [i.e. the number of bytes in the unit corresponding to mag]
            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            // make adjustment when the value is large enough that
            // it would round up to 1000 or more
            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "} {1}",
                adjustedSize,
                SizeSuffixesPersian[mag]);
        }
    }
}
//        public static List<T> ToTree<T>(List<T> list,
//            Func<T, int> keySelect,
//            Func<T, int> parentSelect,
//            Func<T, ICollection<T>> childSelect)
//        {
//            Func<int, List<T>> searchNodes = null;
//            searchNodes = (key) =>
//            {
//                var chs = list.Where(r => parentSelect.Invoke(r) == key).ToList();
//                foreach (var ch in chs)
//                {
//                    var keyValue = keySelect(ch);
//                    var childsNode = childSelect.Invoke(ch);
//                    var childs = searchNodes(keyValue);
//                    foreach (var chi in childs)
//                        childsNode.Add(chi);
//                }

//                return chs;
//            };
//            var ret = searchNodes(0);
//            return ret;
//        }

//        public static void Test()
//        {
//            //var list = new List<tarhcenter.Models.Group>();
//            //ToTree(list,
//            //    l => l.GroupID,
//            //    l => l.ParentID,
//            //    l => l.Childs);
//        }

//    }
//}