﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

#pragma warning disable 162
namespace Tooska.Google
{
	public class Recaptcha
	{
        public const bool Enable = false;
        public const string SiteKey = "6Ldk4Q8UAAAAAOM7hJrLm3kelvLQEKe0N2ZWht8l";
        public const string SecretKey = "6Ldk4Q8UAAAAAJ2A8WE-tJM1AHcYsvm1e1LFRvIK";

        public static bool CheckUser()
        {
            if (!Enable)
                return true;

            var wc = new WebClient();
			var data = new NameValueCollection();
            data.Add("secret", SecretKey);
            data.Add("response", HttpContext.Current.Request.Form["g-recaptcha-response"]);
            data.Add("remoteip", HttpContext.Current.Request.UserHostAddress);
            try {
                var res = wc.UploadValues("https://www.google.com/recaptcha/api/siteverify", data);
                dynamic json = Newtonsoft.Json.JsonConvert.DeserializeObject(System.Text.Encoding.UTF8.GetString(res));

                return (bool)(json.success);
            }
			catch
            {
                return false;
            }
        }
	}
}

public static class TooskaRecaptchaExtensions
{
    public static MvcHtmlString RecaptchaDiv(this HtmlHelper htmlHelper)
    {
        if (!Tooska.Google.Recaptcha.Enable)
            return MvcHtmlString.Create("");

        return MvcHtmlString.Create(string.Format(@"<script src='https://www.google.com/recaptcha/api.js?hl=fa'></script>
			<div class=""g-recaptcha"" data-sitekey=""{0}""></div>", Tooska.Google.Recaptcha.SiteKey));
    }
}
#pragma warning restore 162