﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Tooska;
using Tooska.Attributes;

namespace Tooska
{
    public enum ButtonType
    {
        [Obsolete("Not exists in bootstrap 4")]
        Default,
        Primary,
        Secondary,
        Success,
        Danger,
        Info,
        Warning,
        Link
    }

    public enum MessageType
    {
        Default,
        [Icon(FontAwesomeIcons.Check)]
        Success,
        [Icon(FontAwesomeIcons.Exclamation)]
        Danger,
        [Icon(FontAwesomeIcons.Info)]
        Info,
        [Icon(FontAwesomeIcons.Warning)]
        Warning
    }
}

public static class TooskaHtmlHelper
{

    public static String IconClassName(this HtmlHelper htmlHelper, FontAwesomeIcons icon)
    {
        return IconHelper.IconClassName(icon);
         //return "fa fa-" + Regex.Replace(icon.ToString(), "(\\B[A-Z])", "-$1").ToLower();
    }
    public static String IconClassName(this HtmlHelper htmlHelper, Glyphicons icon)
    {
        return IconHelper.IconClassName(icon);
        //return "glyphicon glyphicon-" + Regex.Replace(icon.ToString(), "(\\B[A-Z])", "-$1").ToLower();
    }
    public static String IconClassName(this HtmlHelper htmlHelper, SimpleIcons icon)
    {
        return IconHelper.IconClassName(icon);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, ButtonType type)
    {
        return htmlHelper.IconSubmit(text, type, FontAwesomeIcons.Check, null);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, ButtonType type, FontAwesomeIcons icon)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), null);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, ButtonType type, Glyphicons icon)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), null);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, ButtonType type, FontAwesomeIcons icon, object htmlAttributes)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, ButtonType type, Glyphicons icon, object htmlAttributes)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, ButtonType type, string className, object htmlAttributes)
    {
        if (!className.StartsWith("fa") && !className.StartsWith("glyphicons"))
            className = "fa fa-check";

        var button = new TagBuilder("button");
        button.Attributes.Add("type", "submit");
        if (htmlAttributes != null)
            button.MergeAttributes(new RouteValueDictionary(htmlAttributes), false);

        button.AddCssClass("btn btn-labeled btn-" + type.ToString().ToLower());
        button.InnerHtml = string.Format(@" <span class=""btn-label""><i class=""{1}""></i></span>{0}",
            text, className);

        //var s = string.Format(@"<button type=""submit"" class=""btn btn-labeled btn-{1}"">
        //        <span class=""btn-label"">
        //            <i class=""fa fa-check""></i>
        //        </span>{0}
        //    </button>", text, type.ToString().ToLower());
        return MvcHtmlString.Create(button.ToString());
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, ButtonType type, string href, FontAwesomeIcons icon)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon));
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, ButtonType type, string href, Glyphicons icon)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon));
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, ButtonType type, string href, FontAwesomeIcons icon, object htmlAttributes)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, ButtonType type, string href, Glyphicons icon, object htmlAttributes)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, ButtonType type, 
        string href, string className = "times", object htmlAttributes = null)
    {
        //TODO: remove this of block
        // this block writen due bc
        //if (!className.StartsWith("fa") && !className.StartsWith("glyphicon"))
        //    className = "fa fa-" + className;

        var tag = new TagBuilder("a");
        if (htmlAttributes != null)
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes), false);

        tag.Attributes.Add("href", href);

        if (string.IsNullOrWhiteSpace(className))
        {
            tag.AddCssClass("btn btn-" + type.ToString().ToLower());
            tag.InnerHtml = text;
        }
        else
        {
            tag.AddCssClass("btn btn-labeled btn-" + type.ToString().ToLower());
            tag.InnerHtml = string.Format(@" <span class=""btn-label""><i class=""{1}""></i></span>{0}",
                text, className);
        }
        //var s = string.Format(@"<a class=""btn btn-labeled btn-{1}"" href=""{2}"">
        //        <span class=""btn-label"">
        //            <i class=""{3}""></i>
        //        </span>{0}
        //    </a>", text, type.ToString().ToLower(), href, className);
        return MvcHtmlString.Create(tag.ToString());
    }

    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, MessageType type)
    {
        return htmlHelper.IconSubmit(text, type, FontAwesomeIcons.Check, null);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, MessageType type, FontAwesomeIcons icon)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), null);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, MessageType type, Glyphicons icon)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), null);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, MessageType type, FontAwesomeIcons icon, object htmlAttributes)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, MessageType type, Glyphicons icon, object htmlAttributes)
    {
        return htmlHelper.IconSubmit(text, type, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconSubmit(this HtmlHelper htmlHelper, string text, MessageType type, string className, object htmlAttributes)
    {
        if (!className.StartsWith("fa") && !className.StartsWith("glyphicons"))
            className = "fa fa-check";

        var button = new TagBuilder("button");
        button.Attributes.Add("type", "submit");
        if (htmlAttributes != null)
            button.MergeAttributes(new RouteValueDictionary(htmlAttributes), false);

        button.AddCssClass("btn btn-labeled btn-" + type.ToString().ToLower());
        button.InnerHtml = string.Format(@" <span class=""btn-label""><i class=""{1}""></i></span>{0}",
            text, className);

        //var s = string.Format(@"<button type=""submit"" class=""btn btn-labeled btn-{1}"">
        //        <span class=""btn-label"">
        //            <i class=""fa fa-check""></i>
        //        </span>{0}
        //    </button>", text, type.ToString().ToLower());
        return MvcHtmlString.Create(button.ToString());
    }

    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, MessageType type, string href, FontAwesomeIcons icon)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon));
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, MessageType type, string href, Glyphicons icon)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon));
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, MessageType type, string href, FontAwesomeIcons icon, object htmlAttributes)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, MessageType type, string href, Glyphicons icon, object htmlAttributes)
    {
        return htmlHelper.IconButton(text, type, href, htmlHelper.IconClassName(icon), htmlAttributes);
    }
    public static MvcHtmlString IconButton(this HtmlHelper htmlHelper, string text, MessageType type,
        string href, string className = "times", object htmlAttributes = null)
    {
        //TODO: remove this of block
        // this block writen due bc
        //if (!className.StartsWith("fa") && !className.StartsWith("glyphicon"))
        //    className = "fa fa-" + className;

        var tag = new TagBuilder("a");
        if (htmlAttributes != null)
            tag.MergeAttributes(new RouteValueDictionary(htmlAttributes), false);

        tag.Attributes.Add("href", href);
        tag.Attributes.Add("role", "button");
        if (string.IsNullOrWhiteSpace(className))
        {
            tag.AddCssClass("btn btn-" + type.ToString().ToLower());
            tag.InnerHtml = text;
        }
        else
        {
            tag.AddCssClass("btn btn-labeled btn-" + type.ToString().ToLower());
            tag.InnerHtml = string.Format(@" <span class=""btn-label""><i class=""{1}""></i></span>{0}",
                text, className);
        }
        //var s = string.Format(@"<a class=""btn btn-labeled btn-{1}"" href=""{2}"">
        //        <span class=""btn-label"">
        //            <i class=""{3}""></i>
        //        </span>{0}
        //    </a>", text, type.ToString().ToLower(), href, className);
        return MvcHtmlString.Create(tag.ToString());
    }

    public static MvcHtmlString BackButton(this HtmlHelper htmlHelper)
    {
        var s = @"<a onclick=""window.history.back();"" type=""submit"" class=""btn btn-labeled btn-default"">
                <span class=""btn-label"">
                    <i class=""fa fa-arrow-left""></i>
                </span>بازگشت
            </a>";

        return MvcHtmlString.Create(s);
    }

    public static MvcHtmlString StyledLabelFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
        if (String.IsNullOrEmpty(labelText))
        {
            return MvcHtmlString.Empty;
        }

        TagBuilder tag = new TagBuilder("label");
        //tag.MergeAttributes(htmlAttributes);
        tag.Attributes.Add("class", "col-md-2 col-form-label control-label");
        tag.InnerHtml = labelText;

        return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
    }

    private static PropertyInfo GetMember(Type t, string fieldName)
    {
        var fields = fieldName.Split('.');

        PropertyInfo p = null;
        foreach (var f in fields)
        {
            p = t.GetProperty(f);
            t = p.GetGetMethod().ReturnType;
        }

        return p;
    }

    public static MvcHtmlString Pager(this HtmlHelper htmlHelper, int page, int totalPages, string queryName = "page")
    {

        var linkFormat = "?" + queryName + "={0}";

        if (!string.IsNullOrWhiteSpace(HttpContext.Current.Request.Url.Query))
        {
            if (string.IsNullOrEmpty(HttpContext.Current.Request.QueryString[queryName]))
                linkFormat = HttpContext.Current.Request.Url.Query + "&" + queryName + "={0}";
            else
                linkFormat = Regex.Replace(HttpContext.Current.Request.Url.Query, "(" + queryName + ")" + @"=\d+", "$1={0}");
        }

        Func<int, string> f = (p) =>
        {
            return string.Format(linkFormat, p);
        };

        var html = @"<ul class=""pagination m0"">";

        if (page != 1)
            html += string.Format(@"<li>
                        <a href=""{0}"" aria-label=""Previous"">
                            <span aria-hidden=""true"">«</span>
                        </a>
                        </li>", f(page - 1));

        int from = Math.Max(1, page - 3);
        int to = Math.Min(totalPages, page + 3);

        if (from != 1)
            html += @"<li><span>...</a></li>";

        for (int i = from; i <= to; i++)
        {
            if (page == i)
                html += string.Format(@"<li class=""active""><span>{0}</a></li>", i);
            else
                html += string.Format(@"<li><a href=""{0}"">{1}</a></li>", f(i), i);
        }

        if (to != totalPages)
            html += @"<li><span>...</a></li>";

        if (page != totalPages)
            html += string.Format(@"<li>
                    <a href=""{0}"" aria-label=""Next"">
                        <span aria-hidden=""true"">»</span>
                    </a>
                    </li>", f(page + 1));
        html += "</ul>";
        return MvcHtmlString.Create(html);
    }

    public static MvcHtmlString EnumDropDown(this HtmlHelper htmlHelper, Type en)
    {
        return htmlHelper.EnumDropDown(en, null);
    }
    public static MvcHtmlString EnumDropDown(this HtmlHelper htmlHelper, Type en, object htmlAttributes)
    {
        var values = Enum.GetValues(en);
        var select = new TagBuilder("select");
        if (htmlAttributes != null)
            select.MergeAttributes(new RouteValueDictionary(htmlAttributes), false);

        foreach (Enum e in values)
        {
            var display = e.Display() ?? e.ToString();
            var value = e.ToString();
            var icon = e.IconClassName();

            if(!string.IsNullOrEmpty(icon))
                icon = "<i class=" + icon + "></i>";
            select.InnerHtml += string.Format("<option value=\"{0}\">{2}{1}</option>", 
                value, display, icon);
        }

        return MvcHtmlString.Create(select.ToString());
    }

    [Obsolete("Error in naming, use right sign name")]
    public static MvcHtmlString StyledDropDownrFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        return htmlHelper.StyledDropDownFor(expression);
    }
    public static MvcHtmlString StyledDropDownFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        //string displayText = metadata.Description ?? metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
        var type = metadata.ModelType;

        //if (type.IsEnum)
        //{
        var names = Enum.GetValues(type);
        StringBuilder sb = new StringBuilder();
        sb.Append(string.Format("<select class=\"form-control\" name=\"{0}\" id=\"{0}\">", htmlFieldName));
        foreach (Enum name in names)
        {
            if (metadata.Model == name)
                sb.Append(string.Format("<option selected=\"selected\" value=\"{0}\">{1}</option>", name, name.Display()));
            else
                sb.Append(string.Format("<option value=\"{0}\">{1}</option>", name, name.Display()));
        }
        sb.Append("</select>");

        return MvcHtmlString.Create(sb.ToString());
        //}
        //else
        //{
        //return htmlHelper.EnumDropDown(expression);
        //}
    }
    public static MvcHtmlString StyledPasswordFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        string displayText = metadata.Description ?? metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
        var type = metadata.ModelType;

        if (String.IsNullOrEmpty(displayText))
            return MvcHtmlString.Empty;

        /*var value = ModelMetadata.FromLambdaExpression(
            expression, htmlHelper.ViewData
        ).Model;*/

        TagBuilder tag = new TagBuilder("input");
        //tag.MergeAttributes(htmlAttributes);


        if (metadata.Model != null)
            tag.Attributes.Add("value", metadata.Model.ToString());

        if (metadata.IsRequired)
            tag.Attributes.Add("required", "required");

        var Info = GetMember(typeof(TModel), htmlFieldName);

        tag.Attributes.Add("type", "password");
        tag.Attributes.Add("placeholder", displayText);
        tag.Attributes.Add("class", "form-control");
        tag.Attributes.Add("name", htmlFieldName);
        tag.MergeAttributes<string, object>(htmlHelper.GetUnobtrusiveValidationAttributes(htmlFieldName, metadata));

        return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));

    }

    public static MvcHtmlString StyledTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        return htmlHelper.StyledTextBoxFor(expression, null);
    }
    public static MvcHtmlString StyledTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        string displayText = metadata.Description ?? metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
        var type = metadata.ModelType;

        if (String.IsNullOrEmpty(displayText))
            return MvcHtmlString.Empty;

        /*var value = ModelMetadata.FromLambdaExpression(
            expression, htmlHelper.ViewData
        ).Model;*/

        var Info = GetMember(typeof(TModel), htmlFieldName);

        TagBuilder tag;

        object[] allowHtmlAttribute = Info.GetCustomAttributes(typeof(AllowHtmlAttribute), false);
        object[] colorAttribute = Info.GetCustomAttributes(typeof(ColorAttribute), false);
        if (allowHtmlAttribute.Length == 0)
        {
            tag = new TagBuilder("input");
            if (metadata.Model != null)
                tag.Attributes.Add("value", metadata.Model.ToString());
        }
        else
        {
            tag = new TagBuilder("textarea");
            if (metadata.Model != null)
                tag.InnerHtml = metadata.Model.ToString();

            tag.AddCssClass("editor");
            Tooska.BundleManager.Register("wysihtml5");
        }

        if (metadata.IsRequired)
            tag.Attributes.Add("required", "required");


        if (Info != null)
        {
            allowHtmlAttribute = Info.GetCustomAttributes /* Info[0].GetCustomAttributes*/(typeof(Tooska.Attributes.MaskAttribute), false);
            if (allowHtmlAttribute.Length != 0)
            {
                var maskString = ((Tooska.Attributes.MaskAttribute)allowHtmlAttribute[0]).Mask;
                tag.Attributes.Add("data-inputmask", "\"mask\": \"" + maskString + "\"");
                tag.Attributes.Add("data-masked", "");
                Tooska.BundleManager.Register("inputmask");
                //tag.Attributes.Add("dir", "ltr");

                maskString = maskString
                    .Replace("9", "x")
                    .Replace("0", "x");
                if (metadata.Description == null)
                    displayText = "به صورت '" + maskString + "' وارد کنید";
            }
        }

        if (metadata.DataTypeName == "Date" || type == typeof(System.DateTime))
        {
            tag.AddCssClass("date-picker");
            Tooska.BundleManager.Register("datepicker");
        }

        if (type == typeof(System.TimeSpan))
        {
            tag.AddCssClass("timespan");
            Tooska.BundleManager.Register("timespaninut");
        }

        if (type == typeof(System.DateTimeOffset))
        {
            tag.AddCssClass("datespan");
            Tooska.BundleManager.Register("timespaninut");
        }

        if (type == typeof(System.Int32))
        {
            tag.Attributes.Add("digits", "true");
            tag.Attributes.Add("data-parsley-type", "integer");
            Tooska.BundleManager.Register("parsley");
        }

        if (colorAttribute.Length > 0)
        {
            tag.Attributes.Add("type", "color");
        }
        else
        {
            if (metadata.DataTypeName == "Password")
                tag.Attributes.Add("type", "password");
            else
                tag.Attributes.Add("type", "text");
        }

        tag.Attributes.Add("placeholder", displayText);
        tag.AddCssClass("form-control");
        tag.Attributes.Add("name", htmlFieldName);
        tag.Attributes.Add("id", htmlFieldName);

        if (htmlAttributes != null)
        {
            var properties = htmlAttributes.GetType().GetProperties();
            var attrs = new Dictionary<string, object>();
            foreach (var p in properties)
                attrs[p.Name.Replace("_", "-")] = p.GetValue(htmlAttributes);

            tag.MergeAttributes(attrs, true);
        }
        tag.MergeAttributes<string, object>(htmlHelper.GetUnobtrusiveValidationAttributes(htmlFieldName, metadata));

        return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
    }

    public static MvcHtmlString StyledDescriptFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);

        if (metadata.Description == null)
            return MvcHtmlString.Create("");
        else
            return MvcHtmlString.Create("<p class=\"help-block\">" + metadata.Description + "</p>");
    }

    public static MvcHtmlString StyledValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        var error = htmlHelper.ViewData.ModelState[htmlFieldName];

        if (error == null)
        {
            return MvcHtmlString.Create("");
        }
        else
        {
            var html = "";
            foreach (var e in error.Errors)
                html += string.Format(
                    @"<span class=""field-validation-error text-danger"" data-valmsg-for=""{0}"" data-valmsg-replace=""true"">{1}</span>",
                    htmlFieldName, e.ErrorMessage);
            return MvcHtmlString.Create(html);
        }
    }

    public static MvcHtmlString StyledValidationSummary<TModel>(this HtmlHelper<TModel> htmlHelper)
    {
        var error = htmlHelper.ViewData.ModelState.SelectMany(m => m.Value.Errors).ToList();

        if (error == null)
        {
            return MvcHtmlString.Create("");
        }
        else
        {
            var html = "<div class=\"validation-summary-errors text-danger\" data-valmsg-summary=\"true\"><ul>";
            foreach (var e in error)
                html += "<li>" + e.ErrorMessage + "</li>";
            html += "</ul></div>";
            return MvcHtmlString.Create(html);
        }
    }


    public static MvcHtmlString StyledUploadFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        return htmlHelper.StyledUploadFor(expression, null);
    }
    public static MvcHtmlString StyledUploadFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        string displayText = metadata.Description ?? metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
        var type = metadata.ModelType;

        if (String.IsNullOrEmpty(displayText))
            return MvcHtmlString.Empty;

        /*var value = ModelMetadata.FromLambdaExpression(
            expression, htmlHelper.ViewData
        ).Model;*/

        var Info = GetMember(typeof(TModel), htmlFieldName);

        TagBuilder tag = new TagBuilder("input");

        tag.Attributes.Add("type", "file");

        if (metadata.IsRequired)
            tag.Attributes.Add("required", "required");

        tag.AddCssClass("form-control");
        tag.Attributes.Add("name", htmlFieldName);
        tag.Attributes.Add("id", htmlFieldName);

        tag.MergeAttributes(new RouteValueDictionary(htmlAttributes), true);

        return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
    }

    public static MvcHtmlString StyledCheckBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
    {
        ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
        string htmlFieldName = ExpressionHelper.GetExpressionText(expression);
        var type = metadata.ModelType;


        /*var value = ModelMetadata.FromLambdaExpression(
            expression, htmlHelper.ViewData
        ).Model;*/


        var val = false;
        bool.TryParse(metadata.Model.ToSafeString(), out val);
        /*
        <label class="switch">
            <input type="checkbox" value="true" class="attr-control">
            <span></span>
        </label>
        */
        var html = string.Format(@"<label class=""switch"">
            <input type=""checkbox"" name=""{0}"" id=""{0}"" value=""true"" {1}>
            <span></span>
        </label>", 
            htmlFieldName, 
            val ? "checked=\"checked\" " : "");

        return MvcHtmlString.Create(html);
    }

    public static MvcHtmlString EnumTemplate(this HtmlHelper t, Type ty, string Template)
    {
        var values = Enum.GetValues(ty);
        string html = "";
        foreach (var v in values)
        {
            html += string.Format(Template,
                v,
                ((Enum)v).Display());
        }
        return MvcHtmlString.Create(html);
    }

    public static MvcHtmlString RenderDebug(this HtmlHelper htmlHelper)
    {
        return MvcHtmlString.Create(BundleManager.Debug());
    }
    public static MvcHtmlString RenderScripts(this HtmlHelper htmlHelper)
    {
        BundleManager.InjectDependencies();
        var deps = BundleManager.GetDependencies()
            .SelectMany(d => d.Files)
            .Where(f => f.ToLower().EndsWith(".js"))
            .ToList();
        var html = "";
        foreach (var js in deps)
            html += "\t <script src=\"" + js + "\"></script>\n";
        return MvcHtmlString.Create(html);
    }

    public static MvcHtmlString RenderJQueryScripts(this HtmlHelper htmlHelper)
    {

        BundleManager.InjectDependencies();
        var deps = BundleManager.GetDependencies()
            .Where(d => !string.IsNullOrEmpty(d.jQueryCode))
            .Select(d => d.jQueryCode)
            .ToList();

        var html = @"
    <script type=""text/javascript"">
        $(document).ready(function() {
"
            + string.Join("\n", deps.ToArray())
        + @"
        });
    </script>";

        return MvcHtmlString.Create(html);
    }

    public static MvcHtmlString RenderStylesheets<TModel>(this HtmlHelper<TModel> htmlHelper)
    {
        BundleManager.InjectDependencies();
        var deps = BundleManager.GetDependencies()
            .SelectMany(d => d.Files)
            .Where(f => f.ToLower().EndsWith(".css"))
            .ToList();
        var html = "";
        foreach (var css in deps)
            html += "\t<link rel=\"stylesheet\" type=\"text/css\" href=\"" + css + "\" />\n";

        return MvcHtmlString.Create(html);
    }

    public static MvcHtmlString RenderSiteMap(this HtmlHelper htmlHelper)
    {
        var sm = Tooska.Utility.SiteMapManager.AlicationInstance.OrderBy(i => i.Order).ToList();
        if (sm.Count == 0)
            return null;

        var html = new StringBuilder("<ol class=\"breadcrumb\">");

        var rootUrl = Tooska.Options.SiteMap.RootUrl;

        if (rootUrl.HasValue)
            sm.Insert(0, rootUrl.Value);

        foreach (var s in sm)
        {
            var icon = "";
            if (s.Icon != null)
                icon = $"<i class=\"{s.Icon.ToString()}\"></i> ";

            if (string.IsNullOrWhiteSpace(s.Url))
                html.AppendFormat("<li class=\"active\">{1}{0}</li>", s.Title, icon);
            else
                html.AppendFormat("<li><a href=\"{0}\">{2}{1}</a></li>", s.Url, s.Title, icon);
        }

        html.Append("</ol>");
        return MvcHtmlString.Create(html.ToString());
    }
}
