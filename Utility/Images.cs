﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;

namespace Tooska.Utility
{
    public class Images
    {
        public enum ResizeMode
        {
            Stretch,
            Center,
            ResizeBySide,
            Crop
        }
        private Image image = null;
        private Image original_image = null;
        private bool _IsValid;

        public bool IsValid
        {
            get
            {
                return _IsValid;
            }
        }

        public object InterpolationMode { get; private set; }

        public Images(Image other)
        {
            original_image = image = other;
        }
        public Images(string imagePath)
        {
            original_image = image = Image.FromFile(imagePath);
        }
        public Images(HttpPostedFileBase uploadedFile)
        {
            _IsValid = false;
            if (uploadedFile == null)
                return;

            if (uploadedFile.ContentLength == 0)
                return;

            if (!uploadedFile.ContentType.ToLower().Contains("image"))
                return;

            original_image = image = Image.FromStream(uploadedFile.InputStream);
            _IsValid = true;
        }

        [Obsolete("PrintStoreMark is obsoleted, use PrintOverlay")]
        public void PrintStoreMark(string markFileName)
        {
            Image StoreMark = Image.FromFile(markFileName);
            Graphics graphic = Graphics.FromImage(image);
            graphic.DrawImage(
                image,
                new Rectangle(0, 0, image.Width, image.Height),
                new Rectangle(0, 0, image.Width, image.Height),
                GraphicsUnit.Pixel
                );
            graphic.DrawImage(StoreMark,
                    new Rectangle(0, image.Height - StoreMark.Height, StoreMark.Width, StoreMark.Height),
                    new Rectangle(0, 0, StoreMark.Width, StoreMark.Height),
                     GraphicsUnit.Pixel);
            graphic.Dispose();
        }

        public void PrintOverlay(string overlayFileName)
        {
            PrintOverlay(overlayFileName, ContentAlignment.TopLeft);
        }
        public void PrintOverlay(string overlayFileName, ContentAlignment align)
        {
            Image waterMark = Image.FromFile(overlayFileName);
            Graphics graphic = Graphics.FromImage(image);
            graphic.DrawImage(
                image,
                new Rectangle(0, 0, image.Width, image.Height),
                new Rectangle(0, 0, image.Width, image.Height),
                GraphicsUnit.Pixel
                );

            int n = (int)align;
            int x = 0;
            int y = 0;
            int m = 0;

            if (n < 0x10)
            {
                y = 0;
                m = n;
            }
            else if (n < 0x100)
            {
                y = (image.Height - waterMark.Height) / 2;
                m = n % 0x10;
            }
            else if (n < 0x1000)
            {
                y = image.Height - waterMark.Height;
                m = n % 0x100;
            }

            if (m == 1)
                x = 0;
            else if (m == 2)
                x = (image.Width - waterMark.Width) / 2;
            else if (m == 4)
                x = image.Width - waterMark.Width;


            graphic.DrawImage(waterMark,
                    new Rectangle(x, y, waterMark.Width, waterMark.Height),
                    new Rectangle(0, 0, waterMark.Width, waterMark.Height),
                     GraphicsUnit.Pixel);
            graphic.Dispose();
        }
        public void PrintOverlayText(string text)
        {
            PrintOverlayText(text, ContentAlignment.TopLeft);
        }
        public void PrintOverlayText(string text, ContentAlignment align)
        {
            Graphics graphic = Graphics.FromImage(image);
            var font = new Font("Tahoma", 14);
            var waterMark = new Rectangle();

            graphic.DrawImage(
                image,
                new Rectangle(0, 0, image.Width, image.Height),
                new Rectangle(0, 0, image.Width, image.Height),
                GraphicsUnit.Pixel
                );

            int n = (int)align;
            int x = 0;
            int y = 0;
            int m = 0;

            if (n < 0x10)
            {
                y = 0;
                m = n;
            }
            else if (n < 0x100)
            {
                y = (image.Height - waterMark.Height) / 2;
                m = n % 0x10;
            }
            else if (n < 0x1000)
            {
                y = image.Height - waterMark.Height;
                m = n % 0x100;
            }

            if (m == 1)
                x = 0;
            else if (m == 2)
                x = (image.Width - waterMark.Width) / 2;
            else if (m == 4)
                x = image.Width - waterMark.Width;


            graphic.DrawString(text, font, Brushes.Black, 
                    new Rectangle(x, y, waterMark.Width, waterMark.Height),
                    StringFormat.GenericDefault);
            graphic.Dispose();
        }

        public void ResizeImage(int newWidth, int newHeight, ResizeMode mode = ResizeMode.Stretch)
        {
            switch (mode)
            {
                case ResizeMode.ResizeBySide:
                    if (newHeight + newWidth == 0)
                        throw new ArgumentException("Exactly one of newWidth or newHeight must be non zero");

                    if (newWidth == 0)
                        ResizeHeight(newHeight);
                    else
                        ResizeWidth(newWidth);
                    break;
                case ResizeMode.Stretch:
                    ResizeImageStretch(newWidth, newHeight);
                    break;
                case ResizeMode.Center:
                    ResizeAspectRatioCenter(newWidth, newHeight);
                    break;
                case ResizeMode.Crop:
                    ResizeAspectRatioCrop(newWidth, newHeight, true);
                    break;
            }
        }

        private void ResizeWidth(int w)
        {
            var h = ((float)w / (float)image.Width) * image.Height;
            ResizeImageStretch(w, (int)h);
        }
        private void ResizeHeight(int h)
        {
            var w = ((float)h / (float)image.Height) * image.Width;
            ResizeImageStretch((int)w, h);
        }
        private void ResizeImageStretch(int newWidth, int newHeight)
        {
            Bitmap new_image = new Bitmap(newWidth, newHeight);
            Graphics g = Graphics.FromImage((Image)new_image);
            g.DrawImage(image, 0, 0, newWidth, newHeight);
            image = new_image;
        }

        private void ResizeAspectRatioCenter(int Width, int Height)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height,
                              PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(image.HorizontalResolution,
                             image.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Red);
            grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(image,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            image = bmPhoto;
        }
        private void ResizeAspectRatioCrop(int Width, int Height, bool needToFill)
        {
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;
            double nScaleW = 0;
            double nScaleH = 0;

            nScaleW = ((double)Width / (double)sourceWidth);
            nScaleH = ((double)Height / (double)sourceHeight);
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (Height - sourceHeight * nScale) / 2;
                destX = (Width - sourceWidth * nScale) / 2;
            }

            if (nScale > 1)
                nScale = 1;

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);


            Bitmap bmPhoto = null;
            
            bmPhoto = new Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            
            using (Graphics grPhoto = Graphics.FromImage(bmPhoto))
            {
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                grPhoto.CompositingQuality = CompositingQuality.HighQuality;
                grPhoto.SmoothingMode = SmoothingMode.HighQuality;

                Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);
                //Console.WriteLine("From: " + from.ToString());
                //Console.WriteLine("To: " + to.ToString());
                grPhoto.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);
                /*
                var r = new Random().Next(10, 100);
                image.Save(@"C:\Users\Administrator\Desktop\" + r.ToString() + "_Original.png");
                bmPhoto.Save(@"C:\Users\Administrator\Desktop\" + r.ToString() + "_Changed.png");
                */
                image = bmPhoto;
            }
        }
        public void ResizeCanvas(int newWidth, int newHeight)
        {
            Bitmap new_image = new Bitmap(newWidth, newHeight);
            Graphics g = Graphics.FromImage((Image)new_image);
            g.Clear(Color.White);
            var xx = (newWidth - image.Width) / 2;
            var yy = (newHeight - image.Height) / 2;
            g.DrawImage(image, xx, yy);
            image = new_image;
        }
        public void Save(string filePath)
        {
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            image.Save(filePath);
        }

        public void ResizeAndSave(int newWidth, int newHeight, string filePath)
        {
            Bitmap new_image = new Bitmap(newWidth, newHeight);
            Graphics g = Graphics.FromImage((Image)new_image);
            g.DrawImage(image, 0, 0, newWidth, newHeight);
            if (System.IO.File.Exists(filePath))
                System.IO.File.Delete(filePath);
            new_image.Save(filePath);
        }

        public void RevertToOriginal()
        {
            image = original_image;
        }
    }

    [Obsolete("The ImageUtilities class has obsolete. Use Images class")]
    public static class ImageUtilities
    {
        //296*228
        private static Image StoreMark = null;

        private static bool LoadStoreMark()
        {
            if (StoreMark == null)
            {
                StoreMark = Image.FromFile(HttpContext.Current.Server.MapPath("~/Content/app/img/logo.png"));
                return true;
            }
            return false;
        }

        public static void ResizeUploadedImage(HttpPostedFileBase image, string savePath, int width, int height)
        {
            Image bmp = Image.FromStream(image.InputStream);
            Bitmap new_image = new Bitmap(width, height);
            Graphics g = Graphics.FromImage((Image)new_image);
            g.DrawImage(bmp, 0, 0, width, height);
            bmp.Save(savePath);
        }

        public static void PrintStoreMark(string fileName)
        {
            //Image img = Image.FromFile(fileName);
            //Bitmap bmp = new Bitmap(fileName);
            ////img.Width, img.Height,                PixelFormat.Format32bppArgb);
            //Graphics g = Graphics.FromImage(bmp);
            //PrintStoreMark(ref bmp, ref g);
            //g.Dispose();
            ////img.Dispose();
            //bmp.Save(fileName);
            //bmp.Dispose();
            LoadStoreMark();
            Image image = Image.FromFile(fileName);
            Bitmap bmPhoto = new Bitmap(image.Width, image.Height, PixelFormat.Format32bppArgb);
            Graphics graphic = Graphics.FromImage(bmPhoto);
            graphic.DrawImage(
                image,
                new Rectangle(0, 0, image.Width, image.Height),
                new Rectangle(0, 0, image.Width, image.Height),
                GraphicsUnit.Pixel
                );
            graphic.DrawImage(StoreMark,
                    new Rectangle(0, image.Height - StoreMark.Height, StoreMark.Width, StoreMark.Height),
                    new Rectangle(0, 0, StoreMark.Width, StoreMark.Height),
                     GraphicsUnit.Pixel);
            graphic.Dispose();
            image.Dispose();
            bmPhoto.Save(fileName);
            bmPhoto.Dispose();
        }

        public static void PrintStoreMark(ref Bitmap image, ref Graphics g)
        {
            LoadStoreMark();
            g.DrawImage(StoreMark,
                new Rectangle(0, 0, StoreMark.Width, StoreMark.Height),
                new Rectangle(0, 0, StoreMark.Width, StoreMark.Height),
                 GraphicsUnit.Pixel);
        }

        public static Image FitImage(HttpPostedFileBase postedFile, int width, int height)
        {
            Image imgPhoto = Image.FromStream(postedFile.InputStream);
            FitImage(imgPhoto, width, height);
            return imgPhoto;
        }
        public static void FitImage(string fileName, int width, int height)
        {
            Image imgPhoto = Image.FromFile(fileName);
            FitImage(imgPhoto, width, height).Save(fileName);
        }
        public static Bitmap FitImage(Image imgPhoto, int width, int height)
        {

            var ratioX = (float)width / (float)imgPhoto.Width;
            var ratioY = (float)height / (float)imgPhoto.Height;

            RectangleF newRect = new RectangleF();
            if (ratioX < ratioY)
            {
                newRect.Width = imgPhoto.Width * ratioX;
                newRect.Height = imgPhoto.Height * ratioX;
            }
            else
            {
                newRect.Width = imgPhoto.Width * ratioY;
                newRect.Height = imgPhoto.Height * ratioY;
            }

            newRect.X = (width - newRect.Width) / 2;
            newRect.Y = (height - newRect.Height) / 2;

            Bitmap bmPhoto = new Bitmap(width, height,
                          PixelFormat.Format32bppArgb);

            bmPhoto.SetResolution(imgPhoto.HorizontalResolution,
                         imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Transparent);
            grPhoto.InterpolationMode =
                System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;


            grPhoto.DrawImage(imgPhoto,
                newRect,
                new Rectangle(0, 0, imgPhoto.Width, imgPhoto.Height),
                GraphicsUnit.Pixel);

            if (StoreMark != null)
                grPhoto.DrawImage(StoreMark,
                    new RectangleF(newRect.X, newRect.Top + newRect.Height - StoreMark.Height, StoreMark.Width, StoreMark.Height),
                    new RectangleF(0, 0, StoreMark.Width, StoreMark.Height),
                     GraphicsUnit.Pixel);

            grPhoto.Dispose();
            imgPhoto.Dispose();

            return bmPhoto;
        }
    }
}